/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/RndmGenerators.h"
#include <memory>

class MuonChamberResponse final {
public:
  MuonChamberResponse( std::unique_ptr<Rndm::Numbers> poisson, double mean )
      : m_poisson{std::move( poisson )}, m_chamberNoiseRate{mean} {}

  int extractNoise() { return ( m_chamberNoiseRate > 0 ) ? static_cast<int>( ( *m_poisson )() ) : 0; }

private:
  std::unique_ptr<Rndm::Numbers> m_poisson;
  double                         m_chamberNoiseRate;
};
