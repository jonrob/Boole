/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <CaloDet/DeCalorimeter.h>
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/CaloAdc.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/IRndmGenSvc.h>
#include <GaudiKernel/RndmGenerators.h>

/** @class CaloPinDigitAlg CaloPinDigitAlg.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-02-06
 */
class CaloPinDigitAlg : public LHCb::DetDesc::ConditionAccessorHolder<GaudiAlgorithm> {
public:
  using ConditionAccessorHolder::ConditionAccessorHolder;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  ConditionAccessor<DeCalorimeter> m_detector{this, "Detector", ""};

  Gaudi::Property<std::vector<double>> m_signal{this, "LedSignal", {100.}};
  Gaudi::Property<std::vector<double>> m_spread{this, "LedSpread", {2.}};
  Gaudi::Property<double>              m_cNoise{this, "CoherentNoise", 0.3};
  Gaudi::Property<double>              m_iNoise{this, "IncoherentNoise", 1.2};
  Gaudi::Property<int>                 m_rate{this, "LedRate", 20};
  Gaudi::Property<bool>                m_separatePinContainer{this, "SeparatePinContainer", false};

  int         m_count{-1};
  std::string m_data;
  std::string m_outputData;

  SmartIF<IRndmGenSvc> m_rndmSvc; ///< random number service
};

DECLARE_COMPONENT( CaloPinDigitAlg )

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloPinDigitAlg::initialize() {
  std::string detectorName = name().substr( 0, 4 );

  {
    // the path to the detector depends on the name of the algorithm instance
    // and the ConditionAccessor key must be set before ConditionAccessorHolder::initialize
    // because of a limitation of the DetDesc implementation
    std::string detLoc;
    if ( "Ecal" == detectorName ) {
      detLoc = DeCalorimeterLocation::Ecal;
    } else if ( "Hcal" == detectorName ) {
      detLoc = DeCalorimeterLocation::Hcal;
    }
    // note that we do not have direct access to ConditionAccessor::m_key, so we have to pass through the property
    if ( m_detector.key().empty() )
      setProperty( "Detector", detLoc ).orThrow( "failed to initialize Detector property", name() );
  }

  return ConditionAccessorHolder::initialize().andThen( [this, &detectorName]() -> StatusCode {
    if ( "Ecal" == detectorName ) {
      m_data       = LHCb::CaloAdcLocation::FullEcal;
      m_outputData = LHCb::CaloAdcLocation::EcalPin;

      if ( m_signal.size() != 1 || m_spread.size() != 1 ) {
        return Error( "Ecal: requires only 1 LED signal per PMT" );
      }

    } else if ( "Hcal" == detectorName ) {
      m_data       = LHCb::CaloAdcLocation::FullHcal;
      m_outputData = LHCb::CaloAdcLocation::HcalPin;

      if ( m_signal.size() > 2 || m_signal.size() < 1 || m_spread.size() != m_signal.size() ) {
        return Error( "Hcal: requires 1 or 2 LED signal per PMT" );
      }

    } else {
      return Error( "Invalid detector Name =  " + detectorName );
    }

    m_rndmSvc = service( "RndmGenSvc" );
    m_count   = -1;
    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloPinDigitAlg::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_count;

  if ( m_count >= m_rate ) m_count = 0;
  if ( msgLevel( MSG::DEBUG ) ) debug() << " Sequence " << m_count << "/" << m_rate << endmsg;

  DetElementRef<DeCalorimeter> calo        = m_detector.get();
  const int                    saturateAdc = calo->adcMax();

  // Init
  Rndm::Numbers normale( m_rndmSvc, Rndm::Gauss( 0.0, 1.0 ) );

  // get the ADCs (normal cells)
  LHCb::CaloAdcs* adcs = get<LHCb::CaloAdcs>( m_data );

  LHCb::CaloAdcs* pinAdcs;
  if ( !m_separatePinContainer ) {
    pinAdcs = adcs;
  } else {
    pinAdcs = new LHCb::CaloAdcs();
    put( pinAdcs, m_outputData );
  }

  // ==============================
  // Create Noise for the pinDiode
  // ==============================
  const CaloVector<CaloPin>& caloPins = calo->caloPins();

  // (in)coherent noise
  Rndm::Numbers rndCNoise( m_rndmSvc, Rndm::Gauss( 0.0, m_cNoise ) ); // coherent noise
  Rndm::Numbers rndINoise( m_rndmSvc, Rndm::Gauss( 0.0, m_iNoise ) ); // incoherent noise
  double        cNoise = rndCNoise();
  for ( CaloVector<CaloPin>::const_iterator iPin = caloPins.begin(); iPin != caloPins.end(); ++iPin ) {
    CaloPin                            pin       = *iPin;
    const LHCb::Detector::Calo::CellID id        = pin.id();
    int                                pinSignal = (int)floor( cNoise + rndINoise() + 0.5 );
    std::vector<int>                   leds      = pin.leds();
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " PIN " << iPin - caloPins.begin() << " id " << id << " pedestal : " << pinSignal << " Leds : " << leds
              << endmsg;
    LHCb::CaloAdc* pinAdc = new LHCb::CaloAdc( id, pinSignal );
    pinAdcs->insert( pinAdc );
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << " Initialized : " << pinAdcs->size() << " ADCs for pin Diode " << endmsg;

  // ================//
  // Create LED data //
  // ================//
  const std::vector<CaloLed>& caloLeds = calo->caloLeds();

  if ( msgLevel( MSG::DEBUG ) ) debug() << " Get " << caloLeds.size() << " LEDs " << endmsg;

  // loop over LEDs
  for ( std::vector<CaloLed>::const_iterator iLed = caloLeds.begin(); iLed != caloLeds.end(); ++iLed ) {
    CaloLed led   = *iLed;
    int     index = led.number();
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Led index " << index << endmsg;
    // LED firing : use m_rate & m_count
    if ( index - m_count < 0 ) continue;
    if ( 0 != m_rate * ( (int)( index - m_count ) / m_rate ) - ( index - m_count ) ) continue;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "---> is fired" << endmsg;
    const std::vector<LHCb::Detector::Calo::CellID>& cells = led.cells();
    // if( !ledIsFired ) continue;
    CaloPin cPin = caloPins[led.pin()];
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "-----> LED id " << led.number() << " => " << cells.size() << " cells "
              << " => PIN id " << led.pin() << " => " << cPin.cells().size() << " cells" << endmsg;

    //
    std::vector<int> data;
    for ( unsigned int irndm = 0; irndm < m_spread.size(); ++irndm ) {
      double signal = m_signal[irndm];
      double spread = m_spread[irndm] * normale();
      data.push_back( (int)floor( signal + spread + 0.5 ) );
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << " -----> Add data value " << data << endmsg;
    // Add data to cells ADCs
    if ( msgLevel( MSG::DEBUG ) ) debug() << cells.size() << " connected cells " << endmsg;
    for ( std::vector<LHCb::Detector::Calo::CellID>::const_iterator iCell = cells.begin(); iCell != cells.end();
          ++iCell ) {
      LHCb::CaloAdc* adc = adcs->object( *iCell );
      if ( NULL == adc ) {
        warning() << " ADC not found for cell " << *iCell << endmsg;
        continue;
      }
      int newAdc = data[0] + adc->adc();

      // Hcal : 2 LEDs -> PMT
      if ( 1 != data.size() ) {
        int index = calo->caloPins()[( *iLed ).pin()].index();
        if ( calo->cellParam( *iCell ).leds().size() == data.size() && (unsigned)index < data.size() ) {
          newAdc = data[index] + adc->adc();
        } else {
          warning() << " # Led signal requested does not match with # LED per PMT " << endmsg;
        }
      }
      if ( newAdc > saturateAdc ) { newAdc = saturateAdc; }
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -----> cell Adc " << *iCell << " : " << adc->adc() << " -> " << newAdc << endmsg;
      adc->setAdc( newAdc );
    }

    // Add data to Corresponding Pin-Diode
    LHCb::Detector::Calo::CellID pin    = ( *iLed ).pin();
    LHCb::CaloAdc*               pinAdc = pinAdcs->object( pin );

    int newAdc = pinAdc->adc() + data[0]; // add data
    if ( 1 != data.size() ) {
      int index = calo->caloPins()[( *iLed ).pin()].index();
      if ( (unsigned)index < data.size() ) {
        newAdc = data[index] + pinAdc->adc();
      } else {
        warning() << " # Led index does not match with LedSignal size " << endmsg;
      }
    }

    if ( newAdc > saturateAdc ) { newAdc = saturateAdc; }
    pinAdc->setAdc( newAdc );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " -----> pin Adc " << newAdc << endmsg;
  }
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << " The End " << endmsg;
  return StatusCode::SUCCESS;
}
