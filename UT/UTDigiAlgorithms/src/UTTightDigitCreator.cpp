/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "LHCbMath/LHCbMath.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Transformer.h"

#include <yaml-cpp/yaml.h>

namespace {
  struct Conditions {
    double digitSig2NoiseThreshold{3.0};
    double clusterSig2NoiseThreshold{4.0};
    double highThreshold{10.0};
  };
} // namespace

/**
 *  Class for clustering in the UT tracker
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
class UTTightDigitCreator : public LHCb::Algorithm::Transformer<
                                LHCb::UTDigits( const LHCb::UTDigits&, DeUTDetector const&, Conditions const& ),
                                LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, DeUTDetector, Conditions>> {

public:
  UTTightDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode     initialize() override;
  LHCb::UTDigits operator()( const LHCb::UTDigits&, DeUTDetector const&, Conditions const& ) const override;

private:
  bool aboveDigitSignalToNoise( const LHCb::UTDigit* aDigit, const DeUTSector& aSector,
                                double digitSig2NoiseThreshold ) const;

  StatusCode loadCutsFromConditions();

  Gaudi::Property<int>         m_maxSize{this, "Size", 4};
  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation",
                                                   "/dd/Conditions/ReadoutConf/UT/ClusteringThresholds"};

  using CutMap = std::map<const DeUTSector*, unsigned int>;
  CutMap m_clusterSig2NoiseCut;
  CutMap m_highSig2NoiseCut;

  ToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

using namespace LHCb;

DECLARE_COMPONENT( UTTightDigitCreator )

UTTightDigitCreator::UTTightDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"InputLocation", UTDigitLocation::UTDigits},
                   {"UTLocation", DeUTDetLocation::location()},
                   {"Conditions", name + "_Conditions"}},
                  {"OutputLocation", UTDigitLocation::UTTightDigits}} {}

StatusCode UTTightDigitCreator::initialize() {
  return Transformer::initialize().andThen( [&]() -> StatusCode {
    addConditionDerivation( {m_conditionLocation}, inputLocation<Conditions>(),
                            [&]( YAML::Node const& cInfo ) -> Conditions {
                              info() << "Loading cuts tagged as " << cInfo["tag"].as<std::string>() << endmsg;
                              return {cInfo["HitThreshold"].as<double>(), cInfo["ConfirmationThreshold"].as<double>(),
                                      cInfo["SpilloverThreshold"].as<double>()};
                            } );
    return StatusCode::SUCCESS;
  } );
}

LHCb::UTDigits UTTightDigitCreator::operator()( const UTDigits& digitCont, DeUTDetector const& det,
                                                Conditions const& conditions ) const {
  UTDigits digitCont_ADCcut;
  for ( const auto& digit : digitCont ) {
    auto aSector = det.findSector( digit->channelID() );
    if ( !aboveDigitSignalToNoise( digit, *aSector, conditions.digitSig2NoiseThreshold ) ) continue;

    // link to the DAQ
    UTDAQID utdaqID = m_readoutTool->channelIDToDAQID( (const Detector::UT::ChannelID)digit->channelID() );
    // why do we check this here? Surely if this fails here/now, it would (should!) not have been
    // possible to insert it into digitCont in the first place?
    if ( utdaqID.id() == UTTell1ID::nullBoard ) {
      // trouble
      err() << "failed to link " << endmsg;
      throw GaudiException( "Failed to create digits", "UTTightDigitCreator", StatusCode::FAILURE );
    }
    // make a digit !
    digitCont_ADCcut.insert( new UTDigit( digit->channelID(), digit->depositedCharge(), utdaqID.id() ),
                             digit->channelID() );
  }
  return digitCont_ADCcut;
}

bool UTTightDigitCreator::aboveDigitSignalToNoise( const UTDigit* aDigit, const DeUTSector& aSector,
                                                   double digitSig2NoiseThreshold ) const {
  const unsigned int threshold = LHCb::Math::round( digitSig2NoiseThreshold * aSector.noise( aDigit->channelID() ) );
  return aDigit->depositedCharge() > threshold;
}
