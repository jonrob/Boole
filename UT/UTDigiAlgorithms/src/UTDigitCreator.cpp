/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCTruth.h"
#include "Event/MCUTDigit.h"
#include "Event/UTDigit.h"
#include "IUTCMSimTool.h"
#include "IUTPedestalSimTool.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/UTDataFunctor.h"
#include "LHCbMath/LHCbMath.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"

#ifdef USE_DD4HEP
#  include <DD4hep/GrammarUnparsed.h>
#endif

#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_erf.h"

using namespace LHCb;

namespace {
  // cache the number of noise strips
  struct NoiseStrips {
    unsigned int number;
  };
} // namespace

/**
 *  Algorithm for creating UTDigits from MCUTDigits. The following processes
 *  are simulated:
 *  - Apply readout inefficiency/dead channels by masking MCUTDigits
 *  - Add gaussian noise to total charge of signal digits.
 *  - Generate flat noise over all readout sectors. The ADC value follows a \
 *    gaussian tail distribution.
 *  - Merge the signal and noise digits in one container.
 *  - Add neighbours to each digit with an ADC value of a gaussian core noise.
 *
 *  @author M.Needham
 *  @author J. van Tilburg
 *  @date   05/01/2006
 */
class UTDigitCreator : public LHCb::Algorithm::Transformer<
                           UTDigits( const MCUTDigits&, DeUTDetector const&, NoiseStrips const& ),
                           LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, DeUTDetector, NoiseStrips>> {

public:
  UTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  UTDigits   operator()( const MCUTDigits& mcDigitCont, DeUTDetector const& det, NoiseStrips const& ) const override;

private:
  using digitPair = std::pair<double, LHCb::Detector::UT::ChannelID>;

  void   genRanNoiseStrips( std::vector<digitPair>& noiseCont, DeUTDetector const& det,
                            NoiseStrips const& noiseStrips ) const;
  void   createDigits( const LHCb::MCUTDigits* mcDigitCont, LHCb::UTDigits* digitCont, DeUTDetector const& det,
                       NoiseStrips const& noiseStrips ) const;
  void   mergeContainers( LHCb::span<const digitPair> noiseCont, LHCb::UTDigits* digitsCont,
                          DeUTDetector const& det ) const;
  void   addNeighbours( LHCb::UTDigits* digitsCont, DeUTDetector const& det ) const;
  double genInverseTail() const;
  double adcValue( double value ) const;

  // smart interface to generators
  SmartIF<IRndmGen> m_uniformDist;
  SmartIF<IRndmGen> m_gaussDist;
  SmartIF<IRndmGen> m_gaussTailDist;

  Gaudi::Property<double> m_tailStart{this, "TailStart", 2.5};
  Gaudi::Property<double> m_saturation{this, "Saturation", 127.};
  Gaudi::Property<bool>   m_addPedestal{this, "addPedestal", false};
  Gaudi::Property<bool>   m_addCommonMode{this, "addCommonMode", false};
  Gaudi::Property<bool>   m_allStrips{this, "allStrips", false};
  Gaudi::Property<bool>   m_useStatusConditions{this, "useStatusConditions", true, "use dead strip info"};

  ToolHandle<IUTPedestalSimTool> m_pedestalTool{this, "pedestalToolName", "UTPedestalSimTool"};
  ToolHandle<IUTCMSimTool>       m_cmTool{this, "cmToolName", "UTCMSimTool"};
};

DECLARE_COMPONENT_WITH_ID( UTDigitCreator, "UTDigitCreator" )

namespace {
  struct Less_by_Channel {
    template <typename Pair>
    bool operator()( const Pair& obj1, const Pair& obj2 ) const {
      return obj1.second < obj2.second;
    }
  };

  template <typename Iterator>
  std::pair<bool, Iterator> findDigit( const Detector::UT::ChannelID& aChan, Iterator first, Iterator last ) {
    first = std::find_if_not( first, last, [&aChan]( const auto& i ) { return i->channelID() < aChan; } );
    return {first != last && ( *first )->channelID() == aChan, first};
  }
} // namespace

double UTDigitCreator::adcValue( double value ) const {
  return LHCb::Math::round( std::min( value, m_saturation.value() ) );
}

UTDigitCreator::UTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"InputLocation", MCUTDigitLocation::UTDigits},
                   {"UTLocation", DeUTDetLocation::location()},
                   {"NoiseStripsLocation", "UTDigitCreatorNoiseStrips"}},
                  {"OutputLocation", UTDigitLocation::UTDigits}} {}

StatusCode UTDigitCreator::initialize() {
  return Transformer::initialize().andThen( [&] {
    m_pedestalTool.setEnabled( m_addPedestal );
    m_cmTool.setEnabled( m_addCommonMode );

    // random numbers generators (flat, gaussian and gaussian tail)
    m_uniformDist   = randSvc()->generator( Rndm::Flat( 0., 1. ) );
    m_gaussDist     = randSvc()->generator( Rndm::Gauss( 0., 1. ) );
    m_gaussTailDist = randSvc()->generator( Rndm::GaussianTail( m_tailStart, 1. ) );

    // cache the number of noise strips
    addConditionDerivation( {DeUTDetLocation::location()}, this->template inputLocation<NoiseStrips>(),
                            [this]( DeUTDetector const& det ) {
                              if ( !m_allStrips ) {
                                double fracOfNoiseStrips = 0.5 * gsl_sf_erfc( m_tailStart / sqrt( 2.0 ) );
                                return NoiseStrips{(unsigned int)( fracOfNoiseStrips * det.nStrip() )};
                              } else {
                                return NoiseStrips{det.nStrip()};
                              }
                            } );
    return StatusCode::SUCCESS;
  } );
}

UTDigits UTDigitCreator::operator()( const MCUTDigits& mcDigitCont, DeUTDetector const& det,
                                     NoiseStrips const& noiseStrips ) const {

  // create UTDigits
  UTDigits digitsCont;
  createDigits( &mcDigitCont, &digitsCont, det, noiseStrips );

  // generate random noise
  std::vector<digitPair> noiseCont;
  genRanNoiseStrips( noiseCont, det, noiseStrips );

  // merge containers
  mergeContainers( noiseCont, &digitsCont, det );

  // resort by channel
  std::stable_sort( digitsCont.begin(), digitsCont.end(), UTDataFunctor::Less_by_Channel<const UTDigit*>() );

  // Ensure that there is neighbours for all strips
  addNeighbours( &digitsCont, det );

  // and finally resort
  std::stable_sort( digitsCont.begin(), digitsCont.end(), UTDataFunctor::Less_by_Channel<const UTDigit*>() );

  // Make the link between digits and mcdigits
  setMCTruth( &digitsCont, &mcDigitCont )
      .orThrow( "Failed to set MCTruth info", "UTDigitCreator" )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  // register UTDigits in the transient data store

  return digitsCont;
}

void UTDigitCreator::genRanNoiseStrips( std::vector<digitPair>& noiseCont, DeUTDetector const& det,
                                        NoiseStrips const& noiseStrips ) const {
  // generate random noise strips
  noiseCont.reserve( noiseStrips.number );
  unsigned int nSector = det.nSectors();

  // create noise strips in loop
  if ( !m_allStrips ) {
    for ( unsigned int iNoiseStrip = 0u; iNoiseStrip < noiseStrips.number; ++iNoiseStrip ) {
      // generate a random readout sector
      int                     iSector = (int)( m_uniformDist->shoot() * nSector );
      auto&                   aSector = det.sector( iSector );
      int                     iStrip  = (int)( m_uniformDist->shoot() * aSector.nStrip() ) + aSector.firstStrip();
      Detector::UT::ChannelID aChan   = aSector.stripToChan( iStrip );

      if ( m_useStatusConditions == false || aSector.isOKStrip( aChan ) == true ) {
        // Generate a ADC value following a gaussian tail distribution
        double ranNoise = aSector.noise( aChan ) * m_gaussTailDist->shoot();
        noiseCont.emplace_back( ranNoise, aChan );
      } // alive strip
    }   // iNoiseStrip
  } else {
    det.applyToAllSectors( [&]( DeUTSector const& sector ) {
      for ( unsigned int iStrip = sector.firstStrip(); iStrip < sector.firstStrip() + sector.nStrip(); ++iStrip ) {
        Detector::UT::ChannelID aChan    = sector.stripToChan( iStrip );
        double                  ranNoise = sector.noise( aChan ) * m_gaussDist->shoot();
        noiseCont.emplace_back( ranNoise, aChan );
      } // iStrip
    } );
  }

  // Sort the noise digits by UTChannelID
  std::stable_sort( noiseCont.begin(), noiseCont.end(), Less_by_Channel() );
}

void UTDigitCreator::createDigits( const MCUTDigits* mcDigitCont, UTDigits* digitsCont, DeUTDetector const& det,
                                   NoiseStrips const& noiseStrips ) const {
  // add gaussian noise to real hit channels + allow xxx% dead channels
  digitsCont->reserve( mcDigitCont->size() + noiseStrips.number );
  auto iterMC = mcDigitCont->begin();
  for ( ; iterMC != mcDigitCont->end(); ++iterMC ) {

    // charge including noise
    auto                              sector      = det.findSector( ( *iterMC )->channelID() );
    const SmartRefVector<MCUTDeposit> depositCont = ( *iterMC )->mcDeposit();
    double                            totalCharge = std::accumulate( depositCont.begin(), depositCont.end(), 0.,
                                          UTDataFunctor::Accumulate_Charge<const MCUTDeposit*>() );
    totalCharge += ( m_gaussDist->shoot() * sector->noise( ( *iterMC )->channelID() ) );

    // sim pedestal
    if ( m_addPedestal ) { totalCharge += m_pedestalTool->pedestal( ( *iterMC )->channelID() ); }

    // sim cm noise
    if ( m_addCommonMode ) { totalCharge += m_cmTool->noise( ( *iterMC )->channelID(), det ); }

    // make digit and add to container.
    digitsCont->insert( new UTDigit( adcValue( totalCharge ) ), ( *iterMC )->channelID() );
  } // iterDigit
}

void UTDigitCreator::mergeContainers( LHCb::span<const digitPair> noiseCont, UTDigits* digitsCont,
                                      DeUTDetector const& det ) const {
  // merge the two containers
  Detector::UT::ChannelID prevChan( 0u );
  auto                    lastIter   = digitsCont->end();
  auto                    cachedIter = digitsCont->begin();

  for ( auto [totalCharge, id] : noiseCont ) {
    auto [found, iter] = findDigit( id, cachedIter, lastIter );
    cachedIter         = iter;
    // if strip was not hit: add noise
    if ( !found && prevChan != id ) {
      // sim pedestal
      if ( m_addPedestal ) totalCharge += m_pedestalTool->pedestal( id );
      // sim cm noise
      if ( m_addCommonMode ) totalCharge += m_cmTool->noise( id, det );
      digitsCont->insert( new UTDigit( adcValue( totalCharge ) ), id ); // does this invalidate cached & last???
    }                                                                   // findDigit
    prevChan = id;
  } // iterNoise
}

void UTDigitCreator::addNeighbours( UTDigits* digitsCont, DeUTDetector const& det ) const {

  std::vector<digitPair> tmpCont;
  for ( auto curDigit = digitsCont->begin(); curDigit != digitsCont->end(); ++curDigit ) {

    // Get left neighbour
    auto                    aSector  = det.findSector( ( *curDigit )->channelID() );
    Detector::UT::ChannelID leftChan = aSector->nextLeft( ( *curDigit )->channelID() );

    // Don't add left neighbour if this neighbour is already hit
    Detector::UT::ChannelID prevDigitChan( 0u );
    if ( curDigit != digitsCont->begin() ) {
      auto prevDigit = std::prev( curDigit );
      prevDigitChan  = ( *prevDigit )->channelID();
    }
    if ( leftChan != prevDigitChan && leftChan != 0u ) { tmpCont.emplace_back( genInverseTail(), leftChan ); }

    // Get right neighbour
    Detector::UT::ChannelID rightChan = aSector->nextRight( ( *curDigit )->channelID() );

    // Don't add right neighbour if this neighbour is already hit
    Detector::UT::ChannelID nextDigitChan( 0u );
    auto                    nextDigit = std::next( curDigit );
    if ( nextDigit != digitsCont->end() ) { nextDigitChan = ( *nextDigit )->channelID(); }
    if ( rightChan != nextDigitChan && rightChan != 0u ) { tmpCont.emplace_back( genInverseTail(), rightChan ); }
  } // iterDigit

  for ( auto [charge, id] : tmpCont ) {
    if ( !digitsCont->object( id ) ) {
      // do better sometimes we can make twice ie we start with 101
      auto aSector = det.findSector( id );
      if ( !m_useStatusConditions || aSector->isOKStrip( id ) ) {
        digitsCont->insert( new UTDigit( adcValue( charge ) ), id );
      } // ok strip
    }
  } // iterP
}

double UTDigitCreator::genInverseTail() const {
  double testVal;
  do { testVal = m_gaussDist->shoot(); } while ( testVal > m_tailStart );
  return testVal;
}
