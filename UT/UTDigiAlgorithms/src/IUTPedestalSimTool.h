/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/UT/ChannelID.h"

#include "GaudiKernel/IAlgTool.h"

/**
 *  Interface Class for charge sharing UT strip
 *
 *  @author M.Needham
 *  @date   14/3/2002
 */
struct IUTPedestalSimTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IUTPedestalSimTool, 1, 0 );

  /// calc sharinh
  virtual double pedestal( const LHCb::Detector::UT::ChannelID& chan ) const = 0;
};
