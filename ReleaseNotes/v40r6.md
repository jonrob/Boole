
2019-10-04 Boole v40r6
===

This version uses Lbcom v30r6, LHCb v50r6, Gaudi v32r2 and LCG_96b with ROOT 6.18.04.

This version is a development release for Run 3 digitisation  

This version is released on `master` branch. The previous release on `master` branch  was Boole `v40r5`.

For the complete set of changes picked up by this release, see lhcb/LHCb/-/tags/v50r6, lhcb/Lbcom/-/tags/v50r6


### New features

- PlatformInfo: add gcc9 platforms, LHCb!2122 (@cattanem)

- RichDet: Support for updated 'new' PMTs with large PMT flag correctly set, LHCb!2097, Lbcom!373 (@jonrob)

### Enhancements

- RichDet improvements for new PMT classes, LHCb!2036 (@jonrob)


### Bug fixes
- Fix GenFSRMerge fails on samples with SpillOver, LHCb!2070 (@dfazzini) [LHCBGAUSS-1775]

- Adapt Lookup table for central UT region to correspond to correct numbering scheme of new geometry, LHCb!2074 (@decianm)



### Code modernisations and cleanups

- Extract UT sector mapping from the geometry rather than hardcoding it, LHCb!2133 (@sponce)

- Rename UTModule with UTStave, LHCb!2037 (@abeiter)  
  This coincides with changes to the DDDB and the SIMCOND database.

- Prefer CaloIndex enum over plain (unsigned) int, LHCb!2132, !227 (@graven)  

- CaloDigitAlg - Default initialize all data members, !226 (@jonrob)   


### Changes to tests

- Add a qmtest to verify the VP raw bank encoder, !225 (@graven)   
