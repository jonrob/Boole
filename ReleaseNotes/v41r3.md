2020-12-12 Boole v41r3
===

This version uses
Lbcom [v31r3](../../../../Lbcom/-/tags/v31r3),
LHCb [v51r3](../../../../LHCb/-/tags/v51r3),
Gaudi [v34r1](../../../../Gaudi/-/tags/v34r1) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Boole [v41r2](../-/tags/v41r2), with the following changes:

### New features ~"new feature"

- ~UT | New UT Encoder for TELL40 data format, !317 (@xuyuan)
- Upstream project highlights :star:
  - ~Decoding ~UT | UT TELL40 encoder/decoder v1r1, LHCb!2631 (@xuyuan)
  - ~RICH ~Conditions | Add RICH Derived condition objects to wrap detector objects, LHCb!2795 (@jonrob)


### Fixes ~"bug fix" ~workaround

- ~Decoding ~UT | Follow LHCb!2852: add const to UT, !325 (@graven)


### Enhancements ~enhancement

- ~VP | Updated VP simulation to have 0.05% bad pixels and info message about simulated radiation damage, !319 (@hcroft) [LHCBGAUSS-2141]
- ~FT | Updated SiPM model to 2017 values, !324 (@lohenry)
- ~FT ~Monitoring | Integrated luminosity info message in the FT attenuation interpolation tool, !322 (@gcavalle)
- ~RICH | RICH - SIN model updates., !309 (@bmalecki)
- Upstream project highlights :star:
  - ~RICH | RICH - SIN model updates., LHCb!2806 (@bmalecki)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~VP | Ref update for Boole!319, !321 (@rcurrie)
- ~UT | Follow LHCb!2854 -- cleanup UT, !326 (@graven)
- ~Calo | Remove use of propsPrint(), !316 (@graven)
- ~Build | Fix CLHEP warning silencing with new CMake configuration, !323 (@clemenci)
- ~Build | Remove unnecessary dependency on obsolete detectors, !318 (@pkoppenb)
