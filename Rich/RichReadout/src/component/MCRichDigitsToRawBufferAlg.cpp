/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi Array properties ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "Event/MCRichDigit.h"
#include "Event/RawEvent.h"

// RICH DAQ
#include "RichFutureDAQ/EncodeTel40Data.h"
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// RICH Utils
#include "RichUtils/RichDAQDefinitions.h"
#include "RichUtils/RichException.h"
#include "RichUtils/RichSmartIDSorter.h"

// STL
#include <algorithm>
#include <memory>
#include <mutex>
#include <string>

namespace Rich::MC::Digi {

  // Import DAQ types
  using namespace Rich::Future::DAQ;

  /**
   *  Algorithm to fill the Raw buffer with RICH information from MCRichDigits.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2003-11-06
   */
  class MCRichDigitsToRawBufferAlg final
      : public LHCb::Algorithm::Consumer<
            void( const LHCb::MCRichDigits&, const LHCb::RawEvent&, const Tel40CableMapping&,
                  const PDMDBEncodeMapping& ),
            LHCb::DetDesc::usesBaseAndConditions<Rich::Future::AlgBase<>, Tel40CableMapping, PDMDBEncodeMapping>> {

  public:
    // framework

    MCRichDigitsToRawBufferAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"MCRichDigitsLocation", LHCb::MCRichDigitLocation::Default},
                     KeyValue{"RawEventLocation", LHCb::RawEventLocation::Default},
                     KeyValue{"Tel40CableMapping", Tel40CableMapping::DefaultConditionKey + "-" + name},
                     KeyValue{"PDMDBEncodeMapping", PDMDBEncodeMapping::DefaultConditionKey + "-" + name}} ) {}

    /// Initialise
    StatusCode initialize() override {

      // base initialise
      auto sc = Consumer::initialize();
      if ( !sc ) return sc;

      // Force enable debug messages.
      // sc = setProperty( "OutputLevel", MSG::DEBUG );

      // derived conditions
      Tel40CableMapping::addConditionDerivation( this );
      PDMDBEncodeMapping::addConditionDerivation( this );

      // check for valid data format version
      if ( Rich::DAQ::MaPMT1 != m_version && Rich::DAQ::StreamSmartIDs != m_version ) {
        error() << "Unknown data format version " << m_version.value() << endmsg;
        return StatusCode::FAILURE;
      }

      return sc;
    }

    /// Execution
    void operator()( const LHCb::MCRichDigits& digits,    //
                     const LHCb::RawEvent&     rawEv,     //
                     const Tel40CableMapping&  tel40Maps, //
                     const PDMDBEncodeMapping& pdmdbMaps ) const override {

      // Boole still operates in a configuration where algorithms need to modify a RawEvent
      // object. So until this is changed need to arrange for non-const access to the input object..
      auto& rawEv_nonconst = *( const_cast<LHCb::RawEvent*>( &rawEv ) );

      // because of the above...
      std::lock_guard lock( m_updateLock );

      // Extract the Smart IDs to fill
      LHCb::RichSmartID::Vector ids;
      ids.reserve( digits.size() );
      for ( const auto dig : digits ) { ids.emplace_back( dig->key() ); }
      // sort
      SmartIDSorter::sortByRegion( ids );

      // Format to use
      auto ver = m_version.value();
      if ( Rich::DAQ::MaPMT1 == ver ) {
        // sanity check mappings are properly initialised
        // also require at least version 1 of the mappings
        if ( !tel40Maps.isInitialised() || !pdmdbMaps.isInitialised() || //
             tel40Maps.version() < 1 || pdmdbMaps.version() < 1 ) {
          if ( m_allowFallback ) {
            ++m_fallbackWarn;
            ver = Rich::DAQ::StreamSmartIDs;
          } else {
            throw Rich::Exception( "Mapping Helpers unitialised. Check DB tags." );
          }
        }
      }

      // Format to use
      if ( Rich::DAQ::MaPMT1 == ver ) {

        // Primary format

        // Data structure to form Tel40 banks
        EncodeTel40 encTel40( tel40Maps, this );

        // loop over IDs
        for ( const auto id : ids ) {

          // get the anode data for this hit
          const auto& anodeData = pdmdbMaps.anodeData( id );
          // Now get the tel40 link data
          const auto& tel40Data = tel40Maps.tel40Data( id, anodeData.pdmdb, anodeData.frame );
          // print
          //_ri_verbo << id << endmsg;
          //_ri_verbo << " -> " << anodeData << " " << tel40Data << endmsg;

          // collect the active frame bits per Tel40 data link
          encTel40.add( tel40Data.sourceID, tel40Data.connector, anodeData.bit );
        }

        // finally loop over the collected data and form the raw banks
        encTel40.fill( rawEv_nonconst, ver );

      } else if ( Rich::DAQ::StreamSmartIDs == ver ) {

        // Simple format that just pipes RichSmartIDs directly to a bank.
        // Does not correspond to any real data format but useful for development

        // Make a new data bank object for each Rich
        DetectorArray<PanelArray<std::vector<std::uint32_t>>> dataBanks;
        // reseve full size for each RICH panel, as best guess worst case scenario
        for ( const auto rich : Rich::detectors() ) {
          for ( const auto side : Rich::sides() ) { dataBanks[rich][side].reserve( ids.size() ); }
        }

        // Fill smartIDs direct from digits
        for ( const auto id : ids ) {
          // save in bank for correct RICH
          dataBanks[id.rich()][id.panel()].emplace_back( id );
          _ri_verbo << "Filled " << id << endmsg;
        }

        // save banks
        int iBank{0};
        for ( const auto rich : Rich::detectors() ) {
          for ( const auto side : Rich::sides() ) {
            const Rich::DAQ::SourceID sid( rich, side, iBank++ );
            rawEv_nonconst.addBank( sid.data(), LHCb::RawBank::Rich, ver, std::move( dataBanks[rich][side] ) );
          }
        }
      }
    }

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;

    /// Count instances when automatically version fallback is taken
    mutable WarningCounter m_fallbackWarn{
        this, "DB tags too old to support realistic PMT data format, falling back to ID streaming format."};

    /// Data Format version
    Gaudi::Property<int> m_version{this, "DataVersion", Rich::DAQ::MaPMT1};

    /// Allow fallback to MCFormat if DB tags tool old for realistic format
    Gaudi::Property<bool> m_allowFallback{this, "AllowFormatFallback", true};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( MCRichDigitsToRawBufferAlg )

} // namespace Rich::MC::Digi
