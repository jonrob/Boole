/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file MCRichDigitSummaryAlg.cpp
 *
 * Implementation file for class : MCRichDigitSummaryAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2004-02-11
 */

#include "Event/MCParticle.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichDigitSummary.h"
#include "LHCbAlgs/Transformer.h"

#include "RichKernel/RichAlgBase.h"

#include "MCInterfaces/IRichMCTruthTool.h"

namespace Rich::MC {

  /**
   *  Algorithm to fill the MCRichDigitSummary objects.
   *  Used to provide direct navigation from RichSmartIDs to MCParticles on the DST,
   *  and also to provide some history information.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2005-10-04
   */
  class MCRichDigitSummaryAlg
      : public LHCb::Algorithm::Transformer<LHCb::MCRichDigitSummarys( LHCb::MCRichDigits const& ),
                                            Gaudi::Functional::Traits::BaseClass_t<Rich::AlgBase>> {
  public:
    MCRichDigitSummaryAlg( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode                initialize() override;
    LHCb::MCRichDigitSummarys operator()( LHCb::MCRichDigits const& ) const override;

  private:
    Gaudi::Property<bool>              m_storeSpill{this, "StoreSpillover", false,
                                       "Flag to turn off storing of spillover summaries"};
    ToolHandle<Rich::MC::IMCTruthTool> m_truth{this, "RichMCTruthTool", "Rich::MC::MCTruthTool/RichMCTruthTool"};
  };

  DECLARE_COMPONENT( MCRichDigitSummaryAlg )

} // namespace Rich::MC

Rich::MC::MCRichDigitSummaryAlg::MCRichDigitSummaryAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {"DigitLocation", LHCb::MCRichDigitLocation::Default},
                   {"DigitSummaryLocation", LHCb::MCRichDigitSummaryLocation::Default} ) {}

StatusCode Rich::MC::MCRichDigitSummaryAlg::initialize() {
  return Transformer::initialize().andThen( [&] {
    if ( !m_storeSpill ) info() << "Will only store MCParticle references for main event" << endmsg;
    return StatusCode::SUCCESS;
  } );
}

LHCb::MCRichDigitSummarys Rich::MC::MCRichDigitSummaryAlg::operator()( LHCb::MCRichDigits const& mcDigits ) const {
  LHCb::MCRichDigitSummarys summaries{};
  // loop over mc digits
  for ( auto const* digit : mcDigits ) {
    if ( !digit->hits().empty() ) {
      // loop over hits for this digit
      for ( auto const& hit : digit->hits() ) {
        // if storing all associations or this is a "main" event hit, add to list
        const bool inMainEvent = hit.history().signalEvent();
        if ( m_storeSpill || inMainEvent ) {
          // Make and insert new summary object
          LHCb::MCRichDigitSummary* summary = new LHCb::MCRichDigitSummary();
          summaries.add( summary );
          summary->setRichSmartID( digit->richSmartID() );
          summary->setMCParticle( hit.mcRichHit()->mcParticle() );
          // Copy history from MCRichDigitHit ( contains simulation and digitisation history )
          summary->setHistory( hit.history() );
        }
      } // loop over hits for current digit
    } else if ( digit->history().signalInducedNoise() ) {
      // special treatment for signal-induced noise digits ( no associated hits / MCParticle)
      // Make and insert new summary object
      LHCb::MCRichDigitSummary* summary = new LHCb::MCRichDigitSummary();
      summaries.add( summary );
      summary->setRichSmartID( digit->richSmartID() );
      // Copy history from MCRichDigit ( it is a digit without associated hits )
      summary->setHistory( digit->history() );
    } else {
      Warning( "MCRichDigit has no MCRichHits associated" ).ignore();
    }
  } // end loop over digits
  return summaries;
}
