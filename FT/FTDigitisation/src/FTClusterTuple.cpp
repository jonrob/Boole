/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FTClusterTuple.h"
#include <Event/MCTrackInfo.h>
#include <GaudiKernel/PhysicalConstants.h>
#include <MCInterfaces/IMCReconstructible.h>

//-----------------------------------------------------------------------------
// Implementation file for class : FTClusterTuple
//
// 2020-05-05 : Sevda Esen
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterTuple )

FTClusterTuple::FTClusterTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocation", LHCb::FTClusterLocation::Default},
                 KeyValue{"HitsLocation", "/Event/MC/FT/MergedHits"},
                 KeyValue{"LinkerLocation", Links::location( std::string( LHCb::FTLiteClusterLocation::Default ) +
                                                             "2MCHitsWithSpillover" )},
                 KeyValue{"FT", DeFTDetectorLocation::Default}} ) {}

//=============================================================================
// Main execution
//=============================================================================
void FTClusterTuple::operator()( const FTClusters& Clusters, const std::array<SpillPair, 4>& spills,
                                 const LHCb::LinksByKey& links, const DeFT& det ) const {

  Tuple tuple   = nTuple( "FTCluster" );
  Tuple mctuple = nTuple( "MCHits" );

  std::set<LHCb::MCHit*> efficientMCHits;

  auto nmax        = 25;
  int  numClusters = Clusters.size();

  MCTrackInfo mcInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

  // Loop over FTCluster
  for ( const auto& cluster : Clusters ) {

    std::vector<float> dXCluster{};
    dXCluster.reserve( nmax );
    std::vector<float> pMCHit{};
    pMCHit.reserve( nmax );
    std::vector<float> ptMCHit{};
    ptMCHit.reserve( nmax );
    std::vector<float> etaMCHit{};
    etaMCHit.reserve( nmax );
    std::vector<int> hitTypes{};
    hitTypes.reserve( nmax );
    std::vector<int> idMCHit{};
    idMCHit.reserve( nmax );
    std::vector<int> motherMCHit{};
    motherMCHit.reserve( nmax );
    std::vector<float> txHit{};
    txHit.reserve( nmax );
    std::vector<float> tyHit{};
    tyHit.reserve( nmax );
    std::vector<float> xHit{};
    xHit.reserve( nmax );
    std::vector<float> yHit{};
    yHit.reserve( nmax );
    std::vector<float> zHit{};
    zHit.reserve( nmax );
    std::vector<float> tHit{};
    tHit.reserve( nmax );

    std::vector<float> phiHit{};
    phiHit.reserve( nmax );
    std::vector<float> thetaHit{};
    thetaHit.reserve( nmax );
    std::vector<float> distHit{};
    distHit.reserve( nmax );
    std::vector<bool>  hasT{};
    std::vector<bool>  hasVeloAndUTAndT{};
    std::vector<bool>  hasVeloAndT{};
    std::vector<bool>  hasUTAndT{};
    std::vector<float> origX{};
    std::vector<float> origY{};
    std::vector<float> origZ{};
    std::vector<int>   origType{};

    LHCb::Detector::FTChannelID chanID = cluster->channelID();

    // retrieve FTClustertoMCHitLink
    auto myClusterToHitLink = InputLinks<ContainedObject, LHCb::MCHit>( links );

    // Get the correct module
    const auto& module = det.findModule( chanID );

    int pseudoChannel = -9999;
    if ( module ) { pseudoChannel = module->pseudoChannel( chanID ); }

    tuple->column( "time", cluster->time() ).ignore();
    tuple->column( "station", to_unsigned( chanID.station() ) ).ignore();
    tuple->column( "layer", to_unsigned( chanID.layer() ) ).ignore();
    tuple->column( "quarter", to_unsigned( chanID.quarter() ) ).ignore();
    tuple->column( "module", to_unsigned( chanID.module() ) ).ignore();
    tuple->column( "sipm", chanID.sipmInModule() ).ignore();
    tuple->column( "channel", chanID.channel() ).ignore();
    tuple->column( "pseudoChannel", pseudoChannel ).ignore();

    // Plots for cluster position resolution
    // Get the correct mat
    const auto& mat        = det.findMat( chanID );
    const auto  mcHitLinks = myClusterToHitLink.from( chanID );

    if ( mcHitLinks.empty() ) {

      hitTypes.push_back( 0 );
      idMCHit.push_back( -9999 );
      motherMCHit.push_back( -9999 );
      dXCluster.push_back( -9999 );
      pMCHit.push_back( -9999 );
      ptMCHit.push_back( -9999 );
      etaMCHit.push_back( -9999 );
      txHit.push_back( -9999 );
      tyHit.push_back( -9999 );
      xHit.push_back( -9999 );
      yHit.push_back( -9999 );
      zHit.push_back( -9999 );
      tHit.push_back( -9999 );
      thetaHit.push_back( -9999 );
      phiHit.push_back( -9999 );
      distHit.push_back( -9999 );
      origX.push_back( -9999 );
      origY.push_back( -9999 );
      origZ.push_back( -9999 );
      origType.push_back( -9999 );
      hasVeloAndUTAndT.push_back( false );
      hasVeloAndT.push_back( false );
      hasUTAndT.push_back( false );
      hasT.push_back( false );
    } else {
      // Loop over all links to MCHits
      for ( const auto& imcHit : mcHitLinks ) {
        const auto mcHit  = imcHit.to();
        const auto mcPart = ( mcHit->mcParticle() );

        int hitType;
        if ( mcHit->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT ) {
          hitType = 1;
        } else if ( mcHit->parent()->registry()->identifier() == "/Event/PrevPrev/" + LHCb::MCHitLocation::FT ) {
          hitType = 2;
        } else if ( mcHit->parent()->registry()->identifier() == "/Event/Prev/" + LHCb::MCHitLocation::FT ) {
          hitType = 3;
        } else if ( mcHit->parent()->registry()->identifier() == "/Event/Next/" + LHCb::MCHitLocation::FT ) {
          hitType = 4;
        } else {
          hitType = -1;
          warning() << "WARNING: invalid hitType found!" << endmsg;
        }

        efficientMCHits.insert( mcHit );

        hitTypes.push_back( hitType );

        idMCHit.push_back( mcPart->particleID().pid() );
        if ( mcPart->mother() )
          motherMCHit.push_back( mcPart->mother()->particleID().pid() );
        else
          motherMCHit.push_back( -9999 );

        origX.push_back( mcPart->originVertex()->position().x() );
        origY.push_back( mcPart->originVertex()->position().y() );
        origZ.push_back( mcPart->originVertex()->position().z() );
        origType.push_back( mcPart->originVertex()->type() );
        pMCHit.push_back( mcPart->p() );
        ptMCHit.push_back( mcPart->pt() );
        etaMCHit.push_back( mcPart->pseudoRapidity() );

        if ( mat ) {
          dXCluster.push_back( mat->distancePointToChannel( mcHit->midPoint(), chanID, cluster->fraction() ) );
          // Plot the angle of the cluster (dx) as function of its size
          Gaudi::XYZPoint localEntry = mat->toLocal( mcHit->entry() );
          Gaudi::XYZPoint localExit  = mat->toLocal( mcHit->exit() );
          float           tx         = ( localExit.x() - localEntry.x() ) / ( localExit.z() - localEntry.z() );
          float           theta      = atan( tx ) * 180 / Gaudi::Units::pi;
          float           ty         = ( localExit.y() - localEntry.y() ) / ( localExit.z() - localEntry.z() );
          float           phi        = atan( ty ) * 180.0f / Gaudi::Units::pi;
          float           dist       = ( localExit - localEntry ).R();

          txHit.push_back( tx );
          tyHit.push_back( ty );
          xHit.push_back( mcHit->entry().x() );
          yHit.push_back( mcHit->entry().y() );
          zHit.push_back( mcHit->entry().z() );
          tHit.push_back( mcHit->time() );
          thetaHit.push_back( theta );
          phiHit.push_back( phi );
          distHit.push_back( dist );
        } else {
          txHit.push_back( -9999 );
          tyHit.push_back( -9999 );
          xHit.push_back( -9999 );
          yHit.push_back( -9999 );
          zHit.push_back( -9999 );
          tHit.push_back( -9999 );
          thetaHit.push_back( -9999 );
          phiHit.push_back( -9999 );
          distHit.push_back( -9999 );
        }
        hasVeloAndUTAndT.push_back( mcInfo.hasVeloAndT( mcPart ) && mcInfo.hasUT( mcPart ) );
        hasVeloAndT.push_back( mcInfo.hasVeloAndT( mcPart ) );
        hasUTAndT.push_back( ( mcInfo.hasT( mcPart ) && mcInfo.hasUT( mcPart ) ) );
        hasT.push_back( mcInfo.hasT( mcPart ) );
      }
    }

    tuple->column( "nClusters", numClusters ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fraction", cluster->fraction() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "isLarge", cluster->isLarge() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "clusterSize", cluster->size() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "resolution", dXCluster, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "pMCHit", pMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "ptMCHit", ptMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "etaMCHit", etaMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "idMCHit", idMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "motherMCHit", motherMCHit, "nMCHits", nmax )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "typeHit", hitTypes, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "txHit", txHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "tyHit", tyHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "xHit", xHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "yHit", yHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "zHit", zHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "tHit", tHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "phiHit", phiHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "thetaHit", thetaHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "distHit", distHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "origX", origX, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "origY", origY, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "origZ", origZ, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "origType", origType, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasVeloAndUTAndT", hasVeloAndUTAndT, "nMCHits", nmax )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasVeloAndT", hasVeloAndT, "nMCHits", nmax )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasUTAndT", hasUTAndT, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasT", hasT, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }

  // main loop over spills and MCHits
  for ( const auto& spill : spills ) {

    const LHCb::MCHits* mchits = spill.second;

    // Check if spill is missing
    if ( mchits == nullptr ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Spillover missing in the loop at " << spill.first << " ns" << endmsg;
      continue;
    }

    for ( auto mcHit : *mchits ) {

      bool isEfficient = efficientMCHits.find( mcHit ) != efficientMCHits.end();
      mctuple->column( "isEfficient", isEfficient ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      const auto mcPart = ( mcHit->mcParticle() );

      int hitType;
      if ( mcHit->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT ) {
        hitType = 1;
      } else if ( mcHit->parent()->registry()->identifier() == "/Event/PrevPrev/" + LHCb::MCHitLocation::FT ) {
        hitType = 2;
      } else if ( mcHit->parent()->registry()->identifier() == "/Event/Prev/" + LHCb::MCHitLocation::FT ) {
        hitType = 3;
      } else if ( mcHit->parent()->registry()->identifier() == "/Event/Next/" + LHCb::MCHitLocation::FT ) {
        hitType = 4;
      } else {
        hitType = -1;
        warning() << "WARNING: invalid hitType found!" << endmsg;
      }

      mctuple->column( "typeHit", hitType ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "time", mcHit->time() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "pMCHit", mcPart->p() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "ptMCHit", mcPart->pt() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "etaMCHit", mcPart->pseudoRapidity() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "xHit", mcHit->entry().x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "yHit", mcHit->entry().y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "idMCHit", mcPart->particleID().pid() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "motherMCHit", mcPart->mother() != nullptr ? mcPart->mother()->particleID().pid() : -9999 )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "origZ", mcPart->originVertex()->position().z() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "origType", mcPart->originVertex()->type() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      mctuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }

  return;
}
