/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/MCFTDigit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <LHCbAlgs/Consumer.h>

/** @class MCFTDigitMonitor MCFTDigitMonitor.h
 *
 *
 *  @author Eric Cogneras, Luca Pescatore
 *  @date   2012-07-05
 */

class MCFTDigitMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCFTDigits&, const DeFT& ),
                                                          LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFT>> {
public:
  MCFTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::MCFTDigits& digits, const DeFT& det ) const override;

private:
  void fillHistograms( const DeFT& det, const LHCb::MCFTDigit* mcDigit, const std::set<const LHCb::MCHit*>& mcHits,
                       const std::string& hitType ) const;
};
