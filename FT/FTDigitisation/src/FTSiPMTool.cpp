/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file FTSiPMTool.cpp
 *
 *  Implementation of class : FTSiPMTool
 *
 *  @author Jacco de Vries, Violaine Bellee, and Julian Wishahi
 *  @date   2017-02-14
 */

#include "FTSiPMTool.h"
#include "MCFTDigitCreator.h" // threshold settings
#include "SiPMResponse.h"     // gainshift & pacific version
#include <GaudiKernel/IRndmGenSvc.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <Math/PdfFunc.h>
#include <Math/ProbFunc.h>
#include <numeric>

// Declaration of the Tool Factory
DECLARE_COMPONENT( FTSiPMTool )

static const InterfaceID IID_FTSiPMTool( "FTSiPMTool", 1, 0 );

//=============================================================================
// Tool initialization
//=============================================================================
StatusCode FTSiPMTool::initialize() {
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // tools
  if ( msgLevel( MSG::DEBUG ) ) debug() << ": initializing SiPMResponse" << endmsg;
  m_SiPMResponse = tool<SiPMResponse>( "SiPMResponse", this );

  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  if ( randSvc == nullptr ) { return Error( "Failed to access Random Generator Service", StatusCode::FAILURE ); }

  sc = m_rndmFlat.initialize( randSvc, Rndm::Flat( 0.0, 1.0 ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Flat generator", sc );
  sc = m_rndmLandau.initialize( randSvc, Rndm::Landau( m_meanLandau, m_widthLandau ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Landau generator", sc );

  m_hepRndmEngnIncptn.setGaudiRndmEngine( randSvc->engine() );
  m_rndmCLHEPPoisson = std::make_unique<CLHEP::RandPoissonQ>( m_hepRndmEngnIncptn, 1.0 );

  // scale thermal noise by total crosstalk
  m_rateThermalNoise = m_ThermalNoise / ( 1.0 + m_probDirectXTalk + m_probDelayedXTalk );

  // Set thermal noise probability in 25 ns window
  m_noiseProb = ( m_rateThermalNoise / m_readoutFrequency ) * ( m_sipmIrradiation / m_referenceIrradiation ) *
                std::pow( 2, ( m_sipmTemperature - m_referenceTemperature ) / m_temperatureCoefficient );

  //=============================================================================
  // Reading in effective noise parameters
  //=============================================================================

  std::vector<double> p0;
  std::vector<double> p1;

  if ( m_SiPMResponse->responsefunction() == "pacific4" ) {
    if ( m_PEThreshold1.value() == 1.5 && m_PEThreshold2.value() == 2.5 && m_PEThreshold3.value() == 4.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00720794, 0.0254307, 0.000474503, 4.77496e-07, 2.5108e-10, 0.0215386} );
      p1.insert( p1.begin(), {3.22954, 3.51195, 4.38762, 5.86713, 7.59096, 3.68478} );
    } else if ( m_PEThreshold1.value() == 1.5 && m_PEThreshold2.value() == 2.5 && m_PEThreshold3.value() == 3.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.379491, 0.0267754, 0.000436412, 4.04182e-07, 2.51158e-10, 0.149768} );
      p1.insert( p1.begin(), {2.61262, 3.49416, 4.41247, 5.91844, 7.57975, 3.20534} );
    } else if ( m_PEThreshold1.value() == 2.5 && m_PEThreshold2.value() == 3.5 && m_PEThreshold3.value() == 4.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00552157, 6.74359e-06, 1.43427e-09, 4.52659e-13, 6.32876e-12, 0.00259679} );
      p1.insert( p1.begin(), {3.38788, 5.10414, 6.94038, 8.31251, 6.63043, 3.71505} );
    } else if ( m_PEThreshold1.value() == 2.5 && m_PEThreshold2.value() == 3.5 && m_PEThreshold3.value() == 5.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {3.32346e-05, 7.67269e-06, 1.13245e-09, 8.59171e-15, 1.5103e-06, 2.08099e-05} );
      p1.insert( p1.begin(), {4.3258, 5.06488, 6.99903, 9.47962, 2.84794, 4.87941} );
    } else {
      warning() << "Effective noise parameters not available!! No noise simulated. " << endmsg;
      p0.insert( p0.begin(), {0, 0, 0, 0, 0, 0} );
      p1.insert( p1.begin(), {0, 0, 0, 0, 0, 0} );
    }
  }

  if ( m_SiPMResponse->responsefunction() == "pacific5q_pz5" ) {
    if ( m_PEThreshold1.value() == 1.5 && m_PEThreshold2.value() == 2.5 && m_PEThreshold3.value() == 4.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.284981, 0.409399, 0.00395029, 6.89594e-06, 9.47956e-09, 0.49435} );
      p1.insert( p1.begin(), {2.46383, 2.75665, 3.73996, 5.04828, 6.47118, 2.83164} );
    } else if ( m_PEThreshold1.value() == 1.5 && m_PEThreshold2.value() == 2.5 && m_PEThreshold3.value() == 3.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {6.25031, 0.408655, 0.00391049, 7.17203e-06, 9.22992e-09, 3.68113} );
      p1.insert( p1.begin(), {1.97668, 2.75197, 3.73903, 5.02978, 6.47477, 2.35792} );
    } else if ( m_PEThreshold1.value() == 2.5 && m_PEThreshold2.value() == 3.5 && m_PEThreshold3.value() == 4.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.235113, 0.00146589, 2.0333e-06, 9.28805e-11, 1.50335e-14, 0.153428} );
      p1.insert( p1.begin(), {2.58342, 3.77002, 5.03691, 7.09069, 8.81151, 2.80113} );
    } else if ( m_PEThreshold1.value() == 2.5 && m_PEThreshold2.value() == 3.5 && m_PEThreshold3.value() == 5.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00946568, 0.00102273, 1.13127e-06, 2.3943e-09, 2.88884e-16, 0.00514476} );
      p1.insert( p1.begin(), {3.08778, 3.8851, 5.2196, 6.06228, 10.1229, 3.56729} );
    } else {
      warning() << "Effective noise parameters not available!! No noise simulated. " << endmsg;
      p0.insert( p0.begin(), {0, 0, 0, 0, 0, 0} );
      p1.insert( p1.begin(), {0, 0, 0, 0, 0, 0} );
    }
  }

  if ( m_SiPMResponse->responsefunction() == "pacific5q_pz6" ) {
    if ( m_PEThreshold1.value() == 1.5 && m_PEThreshold2.value() == 2.5 && m_PEThreshold3.value() == 4.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.0930256, 0.0759345, 0.000627617, 4.6727e-07, 2.04502e-09, 0.0957085} );
      p1.insert( p1.begin(), {2.72018, 3.2095, 4.26998, 5.8252, 6.89003, 3.27303} );
    } else if ( m_PEThreshold1.value() == 1.5 && m_PEThreshold2.value() == 2.5 && m_PEThreshold3.value() == 3.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {2.8924, 0.0801284, 0.000663995, 7.29289e-07, 5.23242e-10, 1.15848} );
      p1.insert( p1.begin(), {2.13368, 3.19246, 4.24953, 5.68602, 7.29637, 2.64726} );
    } else if ( m_PEThreshold1.value() == 2.5 && m_PEThreshold2.value() == 3.5 && m_PEThreshold3.value() == 4.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.0538636, 0.000118291, 5.08479e-08, 3.06742e-12, 1.87791e-06, 0.0313937} );
      p1.insert( p1.begin(), {2.9391, 4.40892, 5.98974, 7.89901, 2.84991, 3.18273} );
    } else if ( m_PEThreshold1.value() == 2.5 && m_PEThreshold2.value() == 3.5 && m_PEThreshold3.value() == 5.5 ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00592561, 0.000160619, 4.24277e-08, 6.79141e-11, 4.78152e-09, 0.00164226} );
      p1.insert( p1.begin(), {3.08554, 4.31038, 6.03317, 6.96397, 4.48837, 3.77228} );
    } else {
      warning() << "Effective noise parameters not available!! No noise simulated. " << endmsg;
      p0.insert( p0.begin(), {0, 0, 0, 0, 0, 0} );
      p1.insert( p1.begin(), {0, 0, 0, 0, 0, 0} );
    }
  }

  m_nThermNoiseChansEff[0] = returnEffNoise( p0[0], p1[0] );
  m_nThermNoiseChansEff[1] = returnEffNoise( p0[1], p1[1] );
  m_nThermNoiseChansEff[2] = returnEffNoise( p0[2], p1[2] );
  m_nThermNoiseChansEff[3] = returnEffNoise( p0[3], p1[3] );

  // Number of photons for 1-,2-,3-,4-size clusters (should be > m_thresholds)
  m_nPhotonsPerSize[0] = int( m_PEThreshold3 ) + 1;
  m_nPhotonsPerSize[1] = int( m_PEThreshold2 ) + 1;
  m_nPhotonsPerSize[2] = int( m_PEThreshold2 ) + 1;
  m_nPhotonsPerSize[3] = int( m_PEThreshold2 ) + 1;

  return sc;
}

//=============================================================================
// Return effective noise function output
//=============================================================================
float FTSiPMTool::returnEffNoise( float p0, float p1 ) const {
  float rate = m_rateThermalNoise;
  float func = p0 * std::pow( rate, p1 );
  return func;
}

//=============================================================================
// Add noise to deposit container
//=============================================================================
void FTSiPMTool::addNoise( const DeFT& det, LHCb::MCFTDeposits* deposits ) const {

  // Thermal noise
  if ( m_simulateEffectiveNoise )
    addThermalNoiseEffective( det, deposits );
  else
    addThermalNoise( det, deposits );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of deposits after adding thermal noise = " << deposits->size() << endmsg;

  // After pulse noise
  if ( m_probAfterpulse > 0.005 ) addProfileNoise( det, deposits, m_probAfterpulse );
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of deposits after adding afterpulses = " << deposits->size() << endmsg;
}

//=============================================================================
// Simulate detailed thermal noise
//=============================================================================
void FTSiPMTool::addThermalNoise( const DeFT& det, LHCb::MCFTDeposits* deposits ) const {
  // Draw poissonian for the number of noise clusters
  int nNoise = m_rndmCLHEPPoisson->fire( avgNumThermNoiseChans( det ) );
  // Loop over all thermal noise PEs
  for ( int i = 0; i < nNoise; ++i ) {
    // Get random channel and time to add 1 PE to
    LHCb::Detector::FTChannelID noiseChannel = det.getRandomChannelFromSeed( m_rndmFlat() );

    // Simulate more than 25ns for the time response
    // and subtract integration time offset per station
    double time = m_rndmFlat() * m_numNoiseWindows * 25. * Gaudi::Units::ns +
                  m_integrationOffsets[to_unsigned( noiseChannel.station() ) - 1];

    std::vector<LHCb::MCFTDeposit*> noiseDeps = makeNoiseDeposits( noiseChannel, 1, time );
    for ( const auto deposit : noiseDeps ) deposits->add( deposit );
  } // end of loop over thermal noise PEs
}

//=============================================================================
// Simulate effective thermal noise
//=============================================================================
void FTSiPMTool::addThermalNoiseEffective( const DeFT& det, LHCb::MCFTDeposits* deposits ) const {

  // First add noise to the channels which have signal deposits
  // Sort the deposits, such that deposits for the same channel are neighbours
  std::stable_sort( deposits->begin(), deposits->end(), LHCb::MCFTDeposit::lowerByChannelID );

  // Loop over signal deposits
  std::vector<LHCb::MCFTDeposit*> tmpDeposits;
  LHCb::Detector::FTChannelID     thisChannel( 0 );
  for ( const auto deposit : *deposits ) {
    if ( deposit->channelID() != thisChannel ) {
      if ( thisChannel != 0 && deposit->channelID() != thisChannel + 1 ) {
        // Generate noise deposits for next channel of previous signal deposit
        if ( thisChannel.channel() != 127u )
          generateNoiseDeposits( LHCb::Detector::FTChannelID{thisChannel + 1}, tmpDeposits );
        if ( deposit->channelID() != thisChannel + 2 && deposit->channelID().channel() != 0u ) {
          // Generate noise deposits for previous channel of this signal deposit
          generateNoiseDeposits( LHCb::Detector::FTChannelID{deposit->channelID() - 1}, tmpDeposits );
        }
      }
      thisChannel = deposit->channelID();
      // Generate noise deposits for the current channel
      generateNoiseDeposits( thisChannel, tmpDeposits );
    }
  }
  for ( const auto deposit : tmpDeposits ) deposits->add( deposit );

  // Add the thermal noise
  // Loop over the possible cluster size
  for ( unsigned int clusSize = 1; clusSize <= 4; ++clusSize ) {
    // Draw poissonian for the number of noise clusters
    int nNoise = m_rndmCLHEPPoisson->fire( m_nThermNoiseChansEff[clusSize - 1] );

    // Loop over the number of noise clusters for this cluster size
    for ( int j = 0; j < nNoise; ++j ) {
      LHCb::Detector::FTChannelID noiseChannel = det.getRandomChannelFromSeed( m_rndmFlat() );
      double time = m_timeOfNoiseHits + m_integrationOffsets[to_unsigned( noiseChannel.station() ) - 1];
      // Create the deposits for this cluster size
      auto firstChannel = LHCb::Detector::FTChannelID{noiseChannel - int( 0.5 * clusSize )};
      for ( auto iChannel = firstChannel; iChannel - firstChannel < clusSize; iChannel.advance() ) {
        // Check if the deposit is still in the same module
        if ( iChannel.globalModuleID() == noiseChannel.globalModuleID() ) {
          std::vector<LHCb::MCFTDeposit*> noiseDeps =
              makeNoiseDeposits( iChannel, m_nPhotonsPerSize[clusSize - 1], time );
          for ( const auto deposit : noiseDeps ) deposits->add( deposit );
        }
      }
    }
  }
}

//=============================================================================
// Simulate noise from occupancy profile
//=============================================================================
void FTSiPMTool::addProfileNoise( const DeFT& det, LHCb::MCFTDeposits* deposits, const float scaleFactor ) const {

  // loop over all 'clusters in the event'
  // multiply amount of generated noise with amount of readout windows to simulate.
  int nClusters = int( m_nClustersInEvent * m_numNoiseWindows * m_scaleLandau );
  for ( int i = 0; i < nClusters; ++i ) {
    // get number of noise PEs for this cluster
    int nNoisePEs = m_rndmLandau() * scaleFactor;

    // Only generate a noise hit if above threshold
    if ( nNoisePEs >= int( m_PEThreshold3 ) ) { // single cluster threshold
      // For simplicity put all charge in single channel
      auto noiseChannel = generateFromOccupancyProfile( det );

      // Loop over all photoelectrons
      for ( int k = 0; k < nNoisePEs; ++k ) {
        // Generate a flat time distribution
        double time = m_rndmFlat() * m_numNoiseWindows * 25 * Gaudi::Units::ns +
                      m_integrationOffsets.value().at( to_unsigned( noiseChannel.station() ) - 1 );
        std::vector<LHCb::MCFTDeposit*> noiseDeps = makeNoiseDeposits( noiseChannel, 1, time );
        for ( const auto deposit : noiseDeps ) deposits->add( deposit );
      } // end of all photoelectrons
    }
  } // end loop clusters
}

//=============================================================================
// Randomly generate noise channel from occupancy map
//=============================================================================
LHCb::Detector::FTChannelID FTSiPMTool::generateFromOccupancyProfile( const DeFT& det ) const {

  // Generate pseudoChannel according to occupancy profile
  // PDF for occupancy profile (x=pseudochannel)
  //   for x<nCut  :  P(x) = f0/nChan + f1/nCut
  //   for x>=nCut :  P(x) = f0/nChan + (1-f0-f1)*exp(-x/alpha)/normExp
  //          with normExp = alpha * (exp(-nCut/alpha) - exp(-nTot/alpha))
  static const float rMax = exp( -float( m_nCut ) / m_alpha );
  static const float rMin = exp( -float( m_nChan ) / m_alpha );

  LHCb::Detector::FTChannelID noiseChannel( 0u );
  while ( noiseChannel.channelID() == 0u ) {
    float rndFlat       = m_rndmFlat(); // Draw flat random number [0,1]
    int   pseudoChannel = 0;

    // flat overall component
    if ( rndFlat < m_f0 ) {
      rndFlat /= m_f0;                          // renormalize rndFlat in range [0,1]
      pseudoChannel = int( m_nChan * rndFlat ); // in range [0,nChan]
    }

    // central flat component
    else if ( rndFlat < m_f0.value() + m_f1.value() ) {
      rndFlat       = ( rndFlat - m_f0 ) / m_f1; // renormalize rndFlat in range [0,1]
      pseudoChannel = int( m_nCut * rndFlat );   // in range [0,nCut]
    }

    // exponential component
    else {
      // renormalize rndFlat in [rMin, rMax]
      rndFlat       = ( rndFlat - m_f0 - m_f1 ) / ( 1. - m_f0 - m_f1 ) * ( rMax - rMin ) + rMin;
      pseudoChannel = int( -log( rndFlat ) * m_alpha ); // in range [nCut,nChan]
    }

    noiseChannel = det.getRandomChannelFromPseudo( pseudoChannel, m_rndmFlat() );
  }

  return noiseChannel;
}

//=============================================================================
// Generate noise deposits for a given channel
//=============================================================================
void FTSiPMTool::generateNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel,
                                        std::vector<LHCb::MCFTDeposit*>&  deposits ) const {
  int nPhotons = m_rndmCLHEPPoisson->fire( m_noiseProb * m_numNoiseWindows );
  for ( int i = 0; i < nPhotons; ++i ) {
    double time = m_rndmFlat() * m_numNoiseWindows * 25. * Gaudi::Units::ns +
                  m_integrationOffsets[to_unsigned( noiseChannel.station() ) - 1];

    std::vector<LHCb::MCFTDeposit*> noiseDeps = makeNoiseDeposits( noiseChannel, 1, time );
    for ( const auto deposit : noiseDeps ) deposits.push_back( deposit );
  }
}

//=============================================================================
// Add a noise deposit to the deposit container
//=============================================================================
std::vector<LHCb::MCFTDeposit*> FTSiPMTool::makeNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel,
                                                               int nPhotons, double time ) const {

  std::vector<LHCb::MCFTDeposit*> deposits;

  // Create noise deposit with direct cross talk
  LHCb::MCFTDeposit* deposit =
      new LHCb::MCFTDeposit( noiseChannel, nullptr, nPhotons + generateDirectXTalk( nPhotons ), time, false );
  // info() << "Number of direct xtalk: "<< generateDirectXTalk( nPhotons ) << endmsg;
  deposits.push_back( deposit );

  // generate delayed XTalk
  int nDelayedXtalk = generateDelayedXTalk( nPhotons );
  for ( int i = 0; i < nDelayedXtalk; ++i ) {
    LHCb::MCFTDeposit* deposit =
        new LHCb::MCFTDeposit( noiseChannel, nullptr, 1, time + generateDelayedXTalkTime(), false );
    deposits.push_back( deposit );
  }

  return deposits;
}

//=============================================================================
// Return SiPM PDE
//=============================================================================
float FTSiPMTool::photonDetectionEfficiency( double wavelength ) const {
  float x = wavelength - m_PDEmodelShift;
  return ( m_PDEmodelp3 * x + m_PDEmodelp2 ) * x * x + m_PDEmodelp0;
}

//=============================================================================
// Check if photon is detected due to SiPM PDE
//=============================================================================
bool FTSiPMTool::sipmDetectsPhoton( double wavelength ) const {
  // PDE of 2014 SiPM
  return photonDetectionEfficiency( wavelength ) > m_rndmFlat();
}
