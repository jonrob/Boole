/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file MCFTMCHitInjector.cpp
 *
 *  Implementation of class : MCFTMCHitInjector
 *
 *  @author Belle, Violaine and Wishahi, Julian
 *  @date   2016-11-26
 */

#include "MCFTMCHitInjector.h"
#include <range/v3/all.hpp>

#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTMCHitInjector )

MCFTMCHitInjector::MCFTMCHitInjector( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}},
                   KeyValue{"OutputLocation", "/Event/MC/FT/InjectorHits"} ) {}

StatusCode MCFTMCHitInjector::initialize() {
  return Transformer::initialize().andThen( [&] {
    for ( const auto&& [station, layer, quarter, module, mat] : ranges::views::zip(
              m_targetFTStations, m_targetFTLayers, m_targetFTQuarters, m_targetFTModules, m_targetFTMats ) ) {
      m_targetFTChannelIDs.emplace_back(
          LHCb::Detector::FTChannelID::StationID{station}, LHCb::Detector::FTChannelID::LayerID{layer},
          LHCb::Detector::FTChannelID::QuarterID{quarter}, LHCb::Detector::FTChannelID::ModuleID{module},
          LHCb::Detector::FTChannelID::MatID{mat}, 0u, 0u );
    }
    return StatusCode::SUCCESS;
  } );
}

std::vector<LHCb::MCHit> MCFTMCHitInjector::operator()( DeFT const& deFT ) const {
  // define photons container
  std::vector<LHCb::MCHit> mchitsCont;
  auto mchitProps = ranges::views::zip( m_propMCHitXs, m_propMCHitYs, m_propMCHitDeltaXs, m_propMCHitDeltaYs,
                                        m_propMCHitEnergies, m_propMCHitMomenta, m_propMCHitTimes );
  mchitsCont.reserve( mchitProps.size() );
  for ( auto targetFTChannelID : m_targetFTChannelIDs ) {
    // info() << "MCHits in: " << targetFTChannelID << endmsg;
    const auto& mat           = deFT.findMat( targetFTChannelID );
    auto        mat_thickness = mat->fibreMatThickness();
    for ( const auto&& [x, y, dx, dy, e, p, t] : mchitProps ) {
      Gaudi::XYZPoint  enPointLoc( x, y, -mat_thickness / 2. );
      Gaudi::XYZVector displVecLoc( dx, dy, mat_thickness );
      auto             enPointGlob  = mat->toGlobal( enPointLoc );
      auto             displVecGlob = mat->toGlobal( displVecLoc );
      LHCb::MCHit      mc_hit{};
      mc_hit.setEntry( enPointGlob );
      mc_hit.setDisplacement( displVecGlob );
      mc_hit.setEnergy( e );
      mc_hit.setP( p );
      mc_hit.setTime( t );
      mc_hit.setSensDetID( deFT.sensitiveVolumeID( mc_hit.midPoint() ) );
      mchitsCont.push_back( mc_hit );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " MCHit : "
                << "entry  " << mc_hit.entry() << ", exit   " << mc_hit.exit() << ", E " << mc_hit.energy() << ", p "
                << mc_hit.p() << ", t " << mc_hit.time() << ", sensDetID " << mc_hit.sensDetID() << endmsg;
      }
    }
  }
  return mchitsCont;
}
