/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/MCFTDeposit.h>
#include <Event/MCHit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <LHCbAlgs/Consumer.h>

/** @class MCFTDepositMonitor MCFTDepositMonitor.h
 *
 *  @author Jeroen van Tilburg, Luca Pescatore
 *  @date   2017-03-23
 */
class MCFTDepositMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCFTDeposits&, const DeFT& ),
                                                            LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFT>> {

public:
  MCFTDepositMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  void operator()( const LHCb::MCFTDeposits& deposits, const DeFT& det ) const override;

private:
  void fillHistograms( const DeFT& det, const LHCb::MCFTDeposit* deposit, const std::string& hitType ) const;
};
