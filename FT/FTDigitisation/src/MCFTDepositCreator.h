/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
/// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Transformer.h"

/// from Event
#include "Event/MCFTDeposit.h"
#include "Event/MCFTPhoton.h"
#include "Event/MCHit.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// from Boole/FT/FTDigitisation
#include "IFTSiPMTool.h"
#include "IMCFTAttenuationTool.h"
#include "IMCFTDistributionChannelTool.h"
#include "IMCFTDistributionFibreTool.h"
#include "IMCFTPhotonTool.h"

typedef std::pair<double, const LHCb::MCHits*> SpillPair;

/**
 *  From the list of MCHits, this algorithm fill a container of FTChannelID
 *  with the photons in each of them, and store it in the
 *  transient data store.
 *
 *
 *  @author DE VRIES Jacco, WISHAHI Julian, BELLEE Violaine, COGNERAS Eric
 *  @date   2016-12-19
 */
class MCFTDepositCreator final
    : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons>(
                                                   std::array<SpillPair, 4> const&, DeFT const& ),
                                               LHCb::DetDesc::usesConditions<DeFT>> {

public:
  MCFTDepositCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode                                        initialize() override;
  std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons> operator()( std::array<SpillPair, 4> const&,
                                                                DeFT const& ) const override;

private:
  void convertHitToPhotons( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset, LHCb::MCFTPhotons& photons,
                            const IMCFTDistributionFibreTool::GeomCache& toolCache ) const;

  void convertPhotonsToDepositsDetailed( const LHCb::MCFTPhotons& photons, LHCb::MCFTDeposits& deposits,
                                         DeFT const& deFT ) const;

  void convertHitToDepositsEffective( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset,
                                      LHCb::MCFTDeposits& deposits ) const;

  // Simulation types
  enum class SimulationType { effective, detailed };
  const std::map<std::string, SimulationType> m_simulationTypes{{"effective", SimulationType::effective},
                                                                {"detailed", SimulationType::detailed}};
  SimulationType                              m_simulationType = SimulationType::detailed;
  Gaudi::Property<std::string>                m_simulationTypeChoice{this, "SimulationType", "detailed",
                                                      "Simulation type (effective, detailed)"};

  ToolHandle<IMCFTAttenuationTool>       m_attenuationTool{this, "AttenuationToolName", "MCFTG4AttenuationTool",
                                                     "Tool describing the attenuation in the fibres"};
  ToolHandle<IMCFTDistributionFibreTool> m_distrFibreTool{
      this, "DistributionFibreToolName", "MCFTDistributionFibreTool",
      "Tool describing the energy distribution in the mat's fibres"};
  ToolHandle<IMCFTPhotonTool>              m_photonTool{this, "PhotonToolName", "MCFTPhotonTool",
                                           "Tool describing the distribution of photon observables"};
  ToolHandle<IMCFTDistributionChannelTool> m_distrChannelTool{this, "DistributionChannelToolName",
                                                              "MCFTDistributionChannelTool",
                                                              "Tool distributing energy/photons to the channels"};
  ToolHandle<IFTSiPMTool>                  m_sipmTool{this, "SiPMToolName", "FTSiPMTool",
                                     "Tool adding the noise deposits and the wavelenght-dependent efficiency"};

  Gaudi::Property<bool> m_simulateNoise{this, "SimulateNoise", true, "Simulate noise"};
  Gaudi::Property<bool> m_simulateIntraChannelXTalk{this, "SimulateIntraChannelXTalk", true,
                                                    "Simulate intra-channel cross-talk"};
  Gaudi::Property<bool> m_simulatePDE{this, "SimulatePDE", false, "Simulate Effect of SiPM PDE"};
  Gaudi::Property<bool> m_simulateChannelXT{this, "SimulateChannelXT", true,
                                            "Simulate Effect of channel to channel cross talk"};

  mutable Gaudi::Accumulators::AveragingCounter<> m_nbOfMCHits{this, "NbOfMCHits"};
  mutable Gaudi::Accumulators::Counter<>          m_nbOfMissedMCHits{this, "NbOfMissedMCHits"};
};
