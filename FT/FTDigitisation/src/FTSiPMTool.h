/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "IFTSiPMTool.h"
#include "MCFTCommonTools.h"
#include "SiPMResponse.h"
#include <CLHEP/Random/RandPoissonQ.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiTool.h>
#include <GaudiKernel/RndmGenerators.h>

/** @class FTSiPMTool FTSiPMTool.h
 *
 *  Tool that adds cross-talk and noise from the SiPM and
 *  the efficiency function for detecting photons.
 *
 *  @author Violaine Belle, Julian Wishahi
 *  @date   2017-02-21
 */

class FTSiPMTool : public extends<GaudiTool, IFTSiPMTool> {

public:
  using base_class::base_class;

  StatusCode initialize() override;

  ///==========================================================================
  /// Main method for simulating noise
  ///==========================================================================
  void addNoise( const DeFT& det, LHCb::MCFTDeposits* depositCont ) const override;

  //=============================================================================
  // Return effective noise function output
  //=============================================================================
  float returnEffNoise( float p0, float p1 ) const override;

  //===========================================================================
  // Return number of direct crosstalk PEs based on signal photons
  //===========================================================================
  int generateDirectXTalk( int nPhotons ) const override {

    float prob_XTalk = nPhotons * m_probDirectXTalk;
    float a0         = ( 1. - 2. * prob_XTalk ) / ( 1. - prob_XTalk );

    if ( m_rndmFlat() <= a0 )
      return 0;
    else if ( m_rndmFlat() <= a0 + prob_XTalk )
      return 1;
    else if ( m_rndmFlat() <= a0 + prob_XTalk * prob_XTalk )
      return 2;
    else if ( m_rndmFlat() <= a0 + prob_XTalk * prob_XTalk * prob_XTalk )
      return 3;
    else if ( m_rndmFlat() <= a0 + prob_XTalk * prob_XTalk * prob_XTalk * prob_XTalk )
      return 4;
    else
      return 5;
  }

  //===========================================================================
  // Generate channel to channel crosstalk deposits
  //===========================================================================

  int generateChannelXTalk() const override { return m_rndmCLHEPPoisson->fire( m_channelXTalkProb ); }

  //===========================================================================
  // Return number of delayed crosstalk PEs based on signal photons
  //===========================================================================
  int generateDelayedXTalk( int nPhotons ) const override {

    float prob_delXTalk = nPhotons * m_probDelayedXTalk;
    float a0            = ( 1. - 2. * prob_delXTalk ) / ( 1. - prob_delXTalk );

    if ( m_rndmFlat() <= a0 )
      return 0;
    else if ( m_rndmFlat() <= a0 + prob_delXTalk )
      return 1;
    else if ( m_rndmFlat() <= a0 + prob_delXTalk * prob_delXTalk )
      return 2;
    else if ( m_rndmFlat() <= a0 + prob_delXTalk * prob_delXTalk * prob_delXTalk )
      return 3;
    else if ( m_rndmFlat() <= a0 + prob_delXTalk * prob_delXTalk * prob_delXTalk * prob_delXTalk )
      return 4;
    else
      return 5;
  }

  //===========================================================================
  // Return (additional) time of delayed crosstalk
  //===========================================================================
  float generateDelayedXTalkTime() const override { return -log( m_rndmFlat() ) * m_delayedXtalkDecayTime; }

  //===========================================================================
  // Return PDE for specific wavelength
  //===========================================================================
  float photonDetectionEfficiency( double wavelength ) const override;

  //===========================================================================
  // Return if SiPM is efficient for this wavelength
  //===========================================================================
  bool sipmDetectsPhoton( double wavelength ) const override;

  //=============================================================================
  // Return thresholds
  //=============================================================================
  double threshold1() const override { return m_PEThreshold1; }

  double threshold2() const override { return m_PEThreshold2; }

  double threshold3() const override { return m_PEThreshold3; }

  int avgNumThermNoiseChans( const DeFT& det ) const { return det.nChannels() * m_noiseProb * m_numNoiseWindows; }

private:
  void addThermalNoise( const DeFT& det, LHCb::MCFTDeposits* depositCont ) const;
  void addThermalNoiseEffective( const DeFT& det, LHCb::MCFTDeposits* depositCont ) const;
  void addProfileNoise( const DeFT& det, LHCb::MCFTDeposits* deposits, const float scaleFactor ) const;
  LHCb::Detector::FTChannelID     generateFromOccupancyProfile( const DeFT& det ) const;
  void                            generateNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel,
                                                         std::vector<LHCb::MCFTDeposit*>&  deposits ) const;
  std::vector<LHCb::MCFTDeposit*> makeNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel, int nPhotons,
                                                     double time ) const;

  /// Value for H2017 October batch
  /// from https://indico.cern.ch/event/674476/contributions/2759450/
  Gaudi::Property<double> m_probDirectXTalk{this, "ProbDirectXTalk", 0.04, "Direct cross-talk probability per pe"};

  /// Value for H2017 October batch
  /// from https://indico.cern.ch/event/674476/contributions/2759450/
  Gaudi::Property<double> m_probDelayedXTalk{this, "ProbDelayedXTalk", 0.02, "Delayed cross-talk probability per pe"};

  /// Estimated from Fig.6 of LHCb-INT-2017-006
  Gaudi::Property<double> m_delayedXtalkDecayTime{this, "DelayedXtalkDecayTime", 17.7 * Gaudi::Units::ns,
                                                  "Parameter for the exponential decay of the delayed X-talk"};

  /// Value for H2017 October batch
  /// from https://indico.cern.ch/event/674476/contributions/2759450/
  Gaudi::Property<double> m_probAfterpulse{this, "AfterpulseProb", 0.00, "Afterpulse probability per pe"};

  /// Number of clusters/event, needed for correlated noise estimates
  /// Estimate from minimum bias data (nu=7.6). Only signal.
  Gaudi::Property<float> m_nClustersInEvent{this, "NClustersInEvent", 3100.,
                                            "Average number of clusters per min.bias event"};

  /// Charge distribution (Landau) of the signal (for correlated noise)
  /// Landau only needs to describe the tail well.
  /// This leads to scale factor for the number of clusters
  Gaudi::Property<float> m_meanLandau{this, "MeanLandau", 17.1, "Mean of Landau for cluster charge of signal in PE"};
  Gaudi::Property<float> m_widthLandau{this, "WidthLandau", 2.34, "Mean of Landau for cluster charge of signal in PE"};
  Gaudi::Property<float> m_scaleLandau{this, "ScaleLandau", 0.64,
                                       "Ratio between integral Landau and total number of clusters"};

  /// Analytic description of the occupancy profile
  Gaudi::Property<int>   m_nCut{this, "NCut", 512, "Boundary pseudoChannel for cutout in occupancy profile"};
  Gaudi::Property<int>   m_nChan{this, "NChan", 512 * 4 * 6, "Total number of pseudoChannels in quadrant"};
  Gaudi::Property<float> m_alpha{this, "Alpha", 2274., "Exponent for falling part of occupancy profile"};
  Gaudi::Property<float> m_f0{this, "Fraction0", 0.438, "Fraction of constant part in occupancy profile"};
  Gaudi::Property<float> m_f1{this, "Fraction1", 0.053, "Fraction of cutout (central) part in occupancy profile"};

  /// Parameters for the thermal noise simulation
  Gaudi::Property<double> m_ThermalNoise{this, "ThermalNoiseRate", 14.,
                                         "Thermal noise rate (pDCR) in MHz at reference temperature"};
  Gaudi::Property<double> m_readoutFrequency{this, "ReadoutFrequency", 40., "Readout frequency in MHz (BX rate)"};
  Gaudi::Property<float>  m_referenceIrradiation{this, "ReferenceIrradiation", 6.,
                                                "Reference irradiation in x 10^11 neq/cm2"};
  Gaudi::Property<float>  m_sipmIrradiation{this, "IrradiationLevel", 6., "Irradiation in x 10^11 neq/cm2"};
  Gaudi::Property<float>  m_referenceTemperature{this, "ReferenceTemperature", -40.,
                                                "Temperature in C corresponding to the given thermal noise rate"};
  Gaudi::Property<float>  m_sipmTemperature{this, "Temperature", -40., "Temperature of the SiPMs in Celsius"};
  Gaudi::Property<float>  m_temperatureCoefficient{this, "TemperatureCoefficient", 10.,
                                                  "Temperature coefficient in K for factor 2 reduction"};

  /// Parameters for simulating the arrival times of the noise hits
  Gaudi::Property<int> m_numNoiseWindows{this, "ThermalNoiseWindows", 2, "Thermal noise readout windows to simulate"};
  Gaudi::Property<std::vector<double>> m_integrationOffsets{
      this,
      "IntegrationOffsets",
      {26 * Gaudi::Units::ns, 28 * Gaudi::Units::ns, 30 * Gaudi::Units::ns},
      "Starting time of integration window T1, T2, T3 for noise simulation"};

  /// (Additional) parameters for the _effective_ thermal noise simulation
  Gaudi::Property<bool>  m_simulateEffectiveNoise{this, "SimulateEffectiveNoise", true,
                                                 "Flag to switch on effective thermal noise simulation"};
  Gaudi::Property<float> m_timeOfNoiseHits{this, "TimeOfNoiseHits", 18. * Gaudi::Units::ns,
                                           "Time of noise hits for effective noise simulation"};
  Gaudi::Property<float> m_effNoiseOverlapProb{this, "EffNoiseOverlapProb", 0.8737,
                                               "Probability for overlap of two noise hits in time "
                                               "(depends on electronics response function)"};
  Gaudi::Property<float> m_delayedXTalkOverlapProb{this, "DelayedXTalkOverlapProb", 0.577,
                                                   "Probability for overlap of delayed Xtalk with signal"
                                                   "(depends on electronics response function)"};
  Gaudi::Property<float> m_channelXTalkProb{
      this, "ChannelXTalkProb", 0.05, "Probability of channel to channel cross talk to each neighbouring channel"};

  Gaudi::Property<float> m_PEThreshold1{this, "PEThreshold1", 1.5, "PE low threshold"};
  Gaudi::Property<float> m_PEThreshold2{this, "PEThreshold2", 2.5, "PE mid threshold"};
  Gaudi::Property<float> m_PEThreshold3{this, "PEThreshold3", 4.5, "PE high threshold"};

  /// PDE-vs-wavelength model. 3rd order polynomial
  Gaudi::Property<float> m_PDEmodelp3{this, "PDEmodelp3", 35.9 * std::pow( Gaudi::Units::um, -3 ),
                                      "PDE-vs-wavelength model, 3rd order"};
  Gaudi::Property<float> m_PDEmodelp2{this, "PDEmodelp2", -11.7 * std::pow( Gaudi::Units::um, -2 ),
                                      "PDE-vs-wavelength model, 2nd order"};
  Gaudi::Property<float> m_PDEmodelp0{this, "PDEmodelp0", 0.431, "PDE-vs-wavelength model, max PDE"};
  Gaudi::Property<float> m_PDEmodelShift{this, "PDEmodelShift", 471 * Gaudi::Units::nm,
                                         "PDE-vs-wavelength model, max wavelength"};

  // Number of noise deposits over total detector, in 'm_rdwindows' readoud windows.
  float                m_noiseProb = 0.0;
  std::array<float, 4> m_nThermNoiseChansEff; ///< Effective noise simulation
  std::array<int, 4>   m_nPhotonsPerSize;     ///< Effective noise simulation
  float                m_rateThermalNoise = 0.0;

  SiPMResponse* m_SiPMResponse = nullptr; ///< pointer to SiPM integrated response function

  // Random number generators
  Rndm::Numbers                        m_rndmFlat;
  Rndm::Numbers                        m_rndmGauss;
  Rndm::Numbers                        m_rndmLandau;
  MCFTCommonTools::HepRndmEngnIncptn   m_hepRndmEngnIncptn;
  std::unique_ptr<CLHEP::RandPoissonQ> m_rndmCLHEPPoisson;
};
