/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Associators/Associators.h>
#include <Detector/FT/FTChannelID.h>
#include <Event/FTLiteCluster.h>
#include <Event/MCHit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <LHCbAlgs/Consumer.h>

/** @class FTLiteClusterMonitor FTLiteClusterMonitor.h
 *
 *
 *  @author Eric Cogneras
 *  @date   2012-07-05
 */

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

class FTLiteClusterMonitor
    : public LHCb::Algorithm::Consumer<void( const FTLiteClusters&, const LHCb::MCHits&, const LHCb::LinksByKey&,
                                             const DeFT& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFT>> {
public:
  FTLiteClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode finalize() override;
  void       operator()( const FTLiteClusters&, const LHCb::MCHits& mcHits, const LHCb::LinksByKey& links,
                   const DeFT& det ) const override;

private:
  Gaudi::Property<float> m_minPforResolution{this, "MinPforResolution", 0.0 * Gaudi::Units::GeV,
                                             "Minimum momentum for resolution plots"};
  Gaudi::Property<float> m_excludeSecondaries{this, "ExcludeSecondaries", true,
                                              "Exclude secondaries from calculation of efficiencies and resolutions"};
};
