/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiMath/GaudiMath.h"

static const InterfaceID IID_SiPMResponse( "SiPMResponse", 1, 0 );

/**
 *  This class describes the SiPM response to a single pulse
 *
 *  @author Maurizio Martinelli
 *  @date   2013-11-12
 */
class SiPMResponse : public LHCb::DetDesc::ConditionAccessorHolder<GaudiTool> {
public:
  static const InterfaceID& interfaceID() { return IID_SiPMResponse; }
  SiPMResponse( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;

  virtual double      response( const double time ) const;
  virtual std::string responsefunction() const { return ( m_electronicsResponse ); };
  virtual double      gainshift() const { return ( m_conditions.get().sipmGainShift ); };

private:
  struct Conditions {
    GaudiMath::SimpleSpline responseSpline;
    double                  tMin{0};
    double                  tMax{0};
    /// calibration of the gain
    double sipmGainShift{0};
  };
  ConditionAccessor<Conditions> m_conditions{this, name() + "_Conditions"};

  Gaudi::Property<float>       m_samplingDt{this, "samplingDt", 0.1 * Gaudi::Units::ns, "Sampling time step"};
  Gaudi::Property<std::string> m_splineType{this, "splineType", "Cspline", "The spline type"};
  Gaudi::Property<std::string> m_electronicsResponse{
      this, "ElectronicsResponse", "pacific5q_pz5",
      "Select which electronics response (pacific) to use {pacific4,pacific5q_pz5,pacific5q_pz5}"};
  Gaudi::Property<double>      m_tshift{this, "Tshift", -100.f, "Time shift (t0) of the response shape [ns]"};
  Gaudi::Property<double>      m_sipmGainShift{this, "SiPMGainShift", 0., "calibration of the gain"};
  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation", "/dd/Conditions/Sim/FT/SiPMResponse"};
};
