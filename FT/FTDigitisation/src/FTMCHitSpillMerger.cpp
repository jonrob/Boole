/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file FTMCHitSpillMerger.cpp
 *
 *  Implementation of class : FTMCHitSpillMerger
 *
 *  @author Jeroen van Tilburg
 *  @date   2017-05-11
 */

#include "FTMCHitSpillMerger.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTMCHitSpillMerger )

FTMCHitSpillMerger::FTMCHitSpillMerger( const std::string& name, ISvcLocator* pSvcLocator )
    : MergingTransformer( name, pSvcLocator,
                          {"InputLocation",
                           {"/Event/PrevPrev/" + LHCb::MCHitLocation::FT, "/Event/Prev/" + LHCb::MCHitLocation::FT,
                            "/Event/" + LHCb::MCHitLocation::FT, "/Event/Next/" + LHCb::MCHitLocation::FT}},
                          {"OutputLocation", "/Event/MC/FT/MergedHits"} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::array<SpillPair, 4> FTMCHitSpillMerger::operator()( const vector_of_const_<LHCb::MCHits*>& mcHitsVector ) const {
  std::array<SpillPair, 4> output;

  int i = 0;
  for ( const auto mcHits : mcHitsVector ) {
    output[i].first    = m_spillTimes[i];
    output[i++].second = mcHits;
  }
  return output;
}
