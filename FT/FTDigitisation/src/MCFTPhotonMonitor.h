/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCFTPhoton.h"
#include "Event/MCHit.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "IFTSiPMTool.h"
#include "IMCFTAttenuationTool.h"
#include "LHCbAlgs/Consumer.h"

/**
 *  @author Violaine Bellee, Julian Wishahi, Luca Pescatore
 *  @date   2016-12-18
 */
class MCFTPhotonMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCFTPhotons& ),
                                                           Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

public:
  MCFTPhotonMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const LHCb::MCFTPhotons& deposits ) const override;

private:
  ToolHandle<IMCFTAttenuationTool> m_attenuationTool{this, "AttenuationToolName", "MCFTG4AttenuationTool",
                                                     "Tool to plot the attenuation maps"};
  ToolHandle<IFTSiPMTool>          m_sipmTool{this, "FTSiPMToolName", "FTSiPMTool"};
};
