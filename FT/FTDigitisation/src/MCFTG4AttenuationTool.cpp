/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "IMCFTAttenuationTool.h"

#include "Kernel/STLExtensions.h"
#include <DetDesc/GenericConditionAccessorHolder.h>

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <yaml-cpp/yaml.h>

namespace {
  struct Conditions {
    unsigned int        nBinsX{0}, nBinsY{0};
    std::vector<double> xEdges, yEdges, effDir, effRef;
  };

  int findBin( LHCb::span<const double> axis, double position ) {
    auto iterator = std::upper_bound( axis.begin(), axis.end(), position );
    // If position is outside of map: use first or last bin.
    if ( iterator == axis.end() )
      --iterator;
    else if ( iterator == axis.begin() )
      ++iterator;
    return iterator - axis.begin() - 1;
  }
} // namespace

/**
 *  Tool that reads attenutation maps from ConDB
 *
 *  @author Martin Bieker based on implementation of M. Demmer and J. Wishahi
 *  @date   2015-01-15
 *  @date   2017-07-11
 */
class MCFTG4AttenuationTool : public extends<LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, IMCFTAttenuationTool> {
public:
  using extends::extends;

  /// Initialize the transmission map
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  IMCFTAttenuationTool::Attenuation attenuation( double x, double y ) const override;

private:
  Gaudi::Property<double> m_mirrorReflectivity{this, "MirrorReflectivity", 0.75, // from LHCb-PUB-2014-020
                                               "Reflectivity of mirror at the end of the fibre mat (0-1)"};
  Gaudi::Property<int>    m_irradiation{this, "Irradiation", 50, "Simulated radiation damage"};
  Gaudi::Property<bool>   m_irradiationLinearModel{this, "IrradiationLinearModel", false,
                                                 "Radiation damage model (default is power-law model)"};
  Gaudi::Property<bool>   m_agingFibres{this, "AgingFibres", false, "Simulate time aging of the fibres"};
  Gaudi::Property<bool>   m_replaceModules{this, "ReplaceModules", false, "Simulate replacement of inner modules"};

  ConditionAccessor<Conditions> m_conditions{this, name() + "_Conditions"};
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTG4AttenuationTool )

// Calculate the attenuation
IMCFTAttenuationTool::Attenuation MCFTG4AttenuationTool::attenuation( double hitXPosition, double hitYPosition ) const {
  hitXPosition = std::abs( hitXPosition );
  hitYPosition = std::abs( hitYPosition );

  auto& conds = m_conditions.get();
  int   binX  = findBin( conds.xEdges, hitXPosition );
  int   binY  = findBin( conds.yEdges, hitYPosition );

  // Hit outside quadrant (should never happen!)
  if ( binX == -1 || binY == -1 ) {
    return {0.0, 0.0};
  } else {
    return {conds.effDir[binY * conds.nBinsX + binX], conds.effRef[binY * conds.nBinsX + binX]};
  }
}

StatusCode MCFTG4AttenuationTool::initialize() {
  return extends::initialize().andThen( [&] {
    std::string conditionsLocation =
        "/dd/Conditions/Calibration/FT/AttenuationMap" + std::to_string( m_irradiation ) + "fb_z_920-940";
    // Select the type of radiation map
    if ( m_irradiationLinearModel ) conditionsLocation += "_linearmodel";
    if ( m_agingFibres ) conditionsLocation += "_aging";
    if ( m_replaceModules ) conditionsLocation += "_replacedmodule";
    info() << "Selecting " << std::to_string( m_irradiation ) << "/fb attenuation map using "
           << std::string( m_irradiationLinearModel ? "linear" : "power-law" ) << " radiation damage model "
           << std::string( m_agingFibres ? "with" : "without" ) << " aging fibres and "
           << std::string( m_replaceModules ? "with" : "without" ) << " replaced center module. " << endmsg;

    addConditionDerivation( {conditionsLocation}, m_conditions.key(), [&]( YAML::Node const& cond ) -> Conditions {
      // get parameters from condition DB
      auto nBinsX = cond["x_n_bins"].as<unsigned int>();
      auto nBinsY = cond["y_n_bins"].as<unsigned int>();
      auto xEdges = cond["x_edges"].as<std::vector<double>>();
      auto yEdges = cond["y_edges"].as<std::vector<double>>();
      std::sort( xEdges.begin(), xEdges.end() );
      std::sort( yEdges.begin(), yEdges.end() );
      auto effDir = cond["eff_dir"].as<std::vector<double>>();
      auto effRef = cond["eff_refl"].as<std::vector<double>>();

      // validate parameters
      if ( nBinsX != xEdges.size() - 1 || nBinsY != yEdges.size() - 1 || nBinsX * nBinsY != effDir.size() ||
           nBinsX * nBinsY != effRef.size() ) {
        throw GaudiException( "MCFTG4AttenuationTool::initialize", "Attenuation Map does not match bin counts",
                              StatusCode::FAILURE );
      }

      std::transform( effRef.begin(), effRef.end(), effRef.begin(),
                      [this]( double val ) { return val * m_mirrorReflectivity; } );
      return {nBinsX, nBinsY, xEdges, yEdges, effDir, effRef};
    } );
    return StatusCode::SUCCESS;
  } );
}
