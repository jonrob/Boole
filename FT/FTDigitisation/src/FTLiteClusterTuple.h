/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Associators/Associators.h>
#include <Detector/FT/FTChannelID.h>
#include <Event/FTLiteCluster.h>
#include <Event/MCHit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiTupleAlg.h>
#include <LHCbAlgs/Consumer.h>

/** @class FTLiteClusterTuple FTLiteClusterTuple.h
 *
 *
 *  @author Emmy Gabriel
 *  @date   2020-11-17
 */

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

typedef std::pair<double, const LHCb::MCHits*> SpillPair;

class FTLiteClusterTuple
    : public LHCb::Algorithm::Consumer<void( const FTLiteClusters&, const std::array<SpillPair, 4>&,
                                             const LHCb::LinksByKey&, const DeFT& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeFT>> {
public:
  FTLiteClusterTuple( const std::string& name, ISvcLocator* pSvcLocator );

  void operator()( const FTLiteClusters&, const std::array<SpillPair, 4>& spills, const LHCb::LinksByKey& links,
                   const DeFT& det ) const override;

  void fillTuple( const std::string& name, const LHCb::Detector::FTChannelID chanID, const float fraction,
                  const float size, const float isLarge, const float charge, const LHCb::LinksByKey& links ) const;
};
