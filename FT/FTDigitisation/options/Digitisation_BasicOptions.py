###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob

from Gaudi.Configuration import *
from Configurables import Boole, LHCbApp, DDDBConf, CondDB

###
#job options
importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/Upgrade-WithTruth.py")
importOptions("$APPCONFIGOPTS/Boole/xdigi.py")

#to enable spillover
importOptions("$APPCONFIGOPTS/Boole/EnableSpillover.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20150522.py")

###########
Boole().EvtMax = 1
Boole().DetectorDigi = ['FT']
Boole().DetectorMoni = ['FT']
Boole().DetectorLink = ['FT']

############
#official tag
CondDB().Tags['DDDB'] = 'upgrade/dddb-20180815'

mag = "MagDown"

if mag is "MagDown":
    CondDB().Tags['SIMCOND'] = 'upgrade/sim-20180530-vc-md100'
else:
    CondDB().Tags['SIMCOND'] = 'upgrade/sim-20180530-vc-mu100'

CondDB().Upgrade = True
Boole().DataType = "Upgrade"

datasetname = "MyDatasetName"
Boole().DatasetName = datasetname

#######
#to select the input
from GaudiConf import IOHelper

## for Bs2phiphi:
filepath = "/eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147"
files = glob.glob(filepath + "/*/*.sim")
IOHelper('ROOT').inputFiles(files, clear=True)

## for minbias:
#filepath  = "/eos/lhcb/wg/SciFi/Simulation/RawBanksv5/MagDown/Sim/1068"
#files     = glob.glob(filepath+"/0/output/*.sim")
#IOHelper('ROOT').inputFiles(files,clear=True)

from Configurables import NTupleSvc
NTupleSvc().Output = [
    "FILE1 DATAFILE= '{}.root' TYP='ROOT' OPT='NEW'".format(datasetname)
]


def addProfile():
    from Configurables import CallgrindProfile
    p = CallgrindProfile('CallgrindProfile')
    p.StartFromEventN = 10
    p.StopAtEventN = 20
    p.DumpAtEventN = 20
    p.DumpName = 'CALLGRIND-OUT'
    GaudiSequencer('DigiFTSeq').Members.insert(0, p)


from Configurables import MCFTDepositCreator, MCFTDigitCreator, FTClusterCreator
from Configurables import MCFTDepositMonitor, MCFTDigitMonitor, FTLiteClusterMonitor
from Configurables import Digi__FTClusterMonitor as FTClusterMonitor
from Configurables import FTMCHitSpillMerger, FTRawBankEncoder, FTRawBankDecoder
from Configurables import FTClusterTuple
from Configurables import FTSiPMTool, SiPMResponse
from Configurables import MCFTG4AttenuationTool


def doIt():

    # add the digitisation algorithms you want to run
    GaudiSequencer("MoniFTSeq").Members = [
        MCFTDepositMonitor(),
        MCFTDigitMonitor(),
        FTLiteClusterMonitor(),
        FTClusterMonitor(),
        FTClusterTuple()
    ]

    # here you can change a parameter from the standard value in Boole if required
    digitCreator = MCFTDigitCreator("MCFTDigitCreator")

    digitCreator.addTool(FTSiPMTool, name="FTSiPMTool")
    digitCreator.FTSiPMTool.ProbDirectXTalk = 0.04
    digitCreator.FTSiPMTool.ProbDelayedXTalk = 0.02

    digitCreator.addTool(SiPMResponse, name="SiPMResponse")
    digitCreator.SiPMResponse.ElectronicsResponse = "pacific5q_pz5"

    depositCreator = MCFTDepositCreator("MCFTDepositCreator")
    depositCreator.addTool(FTSiPMTool("FTSiPMTool"))
    depositCreator.FTSiPMTool.ThermalNoiseRate = 14


appendPostConfigAction(doIt)
