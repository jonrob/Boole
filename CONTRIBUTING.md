# General information

In addition to `master`, this project contains several officialy maintained branches, with names such as `XXX-patches`.
They are all protected, meaning that code cannot be pushed into them directly but only through merge requests (MRs).
This helps with the validation of code prior to making it available in the official branches for future releases.

## Available supported branches

- `master` branch: new developments and updates targeting run 3. Builds on current supported platforms against latest version of Gaudi

- `run2-patches` branch: new developments targeting runs 1+2 digitisation. Builds on current supported platfroms against latest version of Gaudi

- `digi14-patches` branch: for bug fixes to Boole in runs 1+2 simulation production workflows. Builds with gcc49 on slc6


## Where to commit code to

- Bug fixes specific to digi14 should be committed to `digi14-patches` branch.

- Any new developments for Run 1 and Run 2 digitisation should go to the `run2-patches` branch.

- Any changes specific to Run 3 should go to `master`.

In doubt, please get in touch before creating a MR.
