/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// Event
#include "Event/MCHit.h"

/** @class VPDigitCreator VPDigitCreator.h
 *  Makes up fake MCHit banks to allow VPDepositCreator to run on events without Prev and/or PrevPrev
 *
 */

class VPEnsureBanks : public GaudiAlgorithm {
public:
  /// Standard constructor
  VPEnsureBanks( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ){};
  StatusCode execute() override; ///< Algorithm execution

private:
  std::array<std::string, 4> m_inputLocations = {"/Event/PrevPrev/" + LHCb::MCHitLocation::VP,
                                                 "/Event/Prev/" + LHCb::MCHitLocation::VP, "LHCb::MCHitLocation::VP",
                                                 "/Event/Next/" + LHCb::MCHitLocation::VP};
};
DECLARE_COMPONENT( VPEnsureBanks )

StatusCode VPEnsureBanks::execute() {
  for ( auto loc : m_inputLocations ) {
    auto mcBank = getIfExists<LHCb::MCHits>( loc );
    if ( mcBank ) continue; // bank exists, we have nothing to do
    LHCb::MCHits* hits = new LHCb::MCHits();
    put( hits, loc );
  }
  return StatusCode::SUCCESS;
}
