/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCVPDigit.h"
#include "Event/VPDigit.h"
#include "Kernel/VPConstants.h"
#include "Kernel/VPDataFunctor.h"
#include "LHCbAlgs/Transformer.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/RndmGenerators.h"

#include <map>
#include <numeric>

#include <yaml-cpp/yaml.h>

using namespace LHCb;

namespace {
  using Threshold = std::array<double, 208>;
} // namespace

/**
 *  Using MCVPDigits as input, this algorithm simulates the response of the
 *  VeloPix ASIC and produces a VPDigit for each MCVPDigit above threshold.
 *
 *  @author Thomas Britton
 *  @date   2010-07-07
 */
class VPDigitCreator
    : public LHCb::Algorithm::Transformer<LHCb::VPDigits( LHCb::MCVPDigits const&, Threshold const& ),
                                          LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, Threshold>> {

public:
  /// Standard constructor
  VPDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode     initialize() override; ///< Algorithm initialization
  LHCb::VPDigits operator()( LHCb::MCVPDigits const&, Threshold const& ) const override;

private:
  /// Noise in number of electrons
  Gaudi::Property<double> m_electronicNoise{this, "ElectronicNoise", 130.0};

  /// Window in which pixel rise time is registered
  Gaudi::Property<double> m_timingWindow{this, "TimingWindow", 25.};

  /// Offset from z/c arrival that timing window starts
  Gaudi::Property<double> m_timingOffset{this, "TimingOffset", 0.};

  /// Option to simulate masked pixels
  Gaudi::Property<bool> m_maskedPixels{this, "MaskedPixels", true};

  /// Option to simulate noisy pixels
  Gaudi::Property<bool> m_noisyPixels{this, "NoisyPixels", false};

  /// Fraction of masked pixels (100 per tile estimate = 0.05%)
  Gaudi::Property<double> m_fractionMasked{this, "FractionMasked",
                                           100. / ( VP::NChipsPerSensor * VP::NColumns * VP::NRows )};

  /// Fraction of noisy pixels (100e, with a 1000 threshold is <3e-16 noise pixels per event)
  Gaudi::Property<double> m_fractionNoisy{this, "FractionNoisy", 0.};

  /// Flag to activate monitoring histograms or not
  Gaudi::Property<bool> m_monitoring{this, "Monitoring", true};

  /// Number of signal VPDigits created
  mutable Gaudi::Accumulators::StatCounter<> m_numSignalDigitsCreated = {this, "1 Signal VPDigits"};

  /// Number of noise VPDigits created
  mutable Gaudi::Accumulators::StatCounter<> m_numNoiseDigitsCreated = {this, "2 Noise VPDigits"};

  /// Number of VPDigits missed due to masked pixels
  mutable Gaudi::Accumulators::StatCounter<> m_numDigitsKilledMasked = {this, "3 Lost Random bad pixels"};

  /// Number of VPDigits created due to spillover
  mutable Gaudi::Accumulators::StatCounter<> m_numSpillover = {this, "4 Spillover VPDigits"};

  /// Number of VPDigits lost due to timing window
  mutable Gaudi::Accumulators::StatCounter<> m_numLostTiming = {this, "5 Out of time VPDigits"};

  /// Location of the condition with the threshold in e- per sensor
  Gaudi::Property<std::string> m_thresholdConditionName{this, "ThresholdCondition",
                                                        "Conditions/Calibration/VP/VPDigitisationParam/Thresholds"};

  /// Random number generators
  mutable Rndm::Numbers m_gauss;
  mutable Rndm::Numbers m_uniform;
  mutable Rndm::Numbers m_poisson;
};

DECLARE_COMPONENT( VPDigitCreator )

VPDigitCreator::VPDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"InputLocation", LHCb::MCVPDigitLocation::Default}, {"Threshold", name + "_Threshold"}},
                  {"OutputLocation", LHCb::VPDigitLocation::Default}} {}

StatusCode VPDigitCreator::initialize() {
  return Transformer::initialize()
      .andThen( [&] { return m_gauss.initialize( randSvc(), Rndm::Gauss( 0., 1. ) ); } )
      .andThen( [&] { return m_uniform.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } )
      .andThen( [&] {
        if ( !m_noisyPixels ) return StatusCode( StatusCode::SUCCESS ); // nothing else to do
        double averageNoisy = m_fractionNoisy * VP::NSensors * VP::NPixelsPerSensor;
        if ( averageNoisy < 1 ) {
          m_noisyPixels = false;
          return StatusCode( StatusCode::SUCCESS );
        }
        return m_poisson.initialize( randSvc(), Rndm::Poisson( averageNoisy ) );
      } )
      .andThen( [&] {
        addConditionDerivation( {m_thresholdConditionName}, inputLocation<Threshold>(),
                                [&]( YAML::Node const& cond ) -> Threshold {
                                  return cond["ThresholdPerSensor"].as<std::array<double, 208>>();
                                } );
        return StatusCode( StatusCode::SUCCESS );
      } )
      .andThen( [&] { setHistoTopDir( "VP/" ); } );
}

LHCb::VPDigits VPDigitCreator::operator()( LHCb::MCVPDigits const& mcdigits, Threshold const& threshold ) const {
  // Create a container for the digits and transfer ownership to the TES.
  LHCb::VPDigits digits;
  unsigned int   numSpillover           = 0;
  unsigned int   numLostTiming          = 0;
  unsigned int   numDigitsKilledMasked  = 0;
  unsigned int   numSignalDigitsCreated = 0;
  unsigned int   numNoiseDigitsCreated  = 0;

  // Loop over the MC digits.
  for ( const LHCb::MCVPDigit* mcdigit : mcdigits ) {
    // Sum up the collected charge from all deposits, in the time windows
    double     chargeMain     = 0.;
    const auto chargeAndTimes = mcdigit->depositAndTimes();
    for ( unsigned int iDep = 0; iDep < chargeAndTimes.size(); ++iDep ) {
      auto depTime = chargeAndTimes[iDep].second + m_timingOffset;
      if ( depTime > 0. && depTime < m_timingWindow ) {
        // is this in the main bunch
        chargeMain += chargeAndTimes[iDep].first;
        // if no MCHit then assume spillover
        if ( !mcdigit->mcHits()[iDep].data() ) ++numSpillover;
      } else {
        // if has MCHit but out of time window
        if ( mcdigit->mcHits()[iDep].data() ) ++numLostTiming;
      }
    }
    const LHCb::Detector::VPChannelID channel = mcdigit->channelID();

    if ( m_monitoring ) {
      plot1D( chargeMain, "MainChargeBefore", "Pixel Charge in main BX, e- before threshold", 0, 50000, 100 );
      plot2D( to_unsigned( channel.sensor() ), chargeMain, "Sensor verse Pixel Charge before threshold (e-)", -0.5,
              207.5, 0, 50000, 208, 100 );
    }
    // Check if the collected charge exceeds the threshold.
    if ( chargeMain < threshold[to_unsigned( channel.sensor() )] + m_electronicNoise * m_gauss() ) continue;

    if ( m_monitoring ) {
      plot1D( chargeMain, "MainChargeAfter", "Pixel harge in main BX, e- after threshold", 0, 50000, 100 );
      plot2D( to_unsigned( channel.sensor() ), chargeMain, "Sensor verse Pixel Charge after threshold (e-)", -0.5,
              207.5, 0, 50000, 208, 100 );
    }
    if ( m_maskedPixels ) {
      // Draw a random number to choose if this pixel is masked.
      if ( m_uniform() < m_fractionMasked ) {
        ++numDigitsKilledMasked;
        continue;
      }
    }
    // Create a new digit.
    digits.insert( new LHCb::VPDigit(), channel );
    ++numSignalDigitsCreated; // all digits from tracks (in any bunch)
  }
  if ( m_noisyPixels ) {
    const auto numToAdd = m_poisson();
    // Add additional digits without a corresponding MC digit.
    for ( unsigned int i = 0; i < numToAdd; ++i ) {
      // Sample the sensor, chip, column and row of the noise pixel.
      const auto sensor = LHCb::Detector::VPChannelID::SensorID( int( floor( m_uniform() * VP::NSensors ) ) );
      const auto chip   = LHCb::Detector::VPChannelID::ChipID( int( floor( m_uniform() * VP::NChipsPerSensor ) ) );
      const auto col    = LHCb::Detector::VPChannelID::ColumnID( int( floor( m_uniform() * VP::NColumns ) ) );
      const auto row    = LHCb::Detector::VPChannelID::RowID( int( floor( m_uniform() * VP::NRows ) ) );
      LHCb::Detector::VPChannelID channel( sensor, chip, col, row );
      // Make sure the channel has not yet been added to the container.
      if ( digits.object( channel ) ) continue;
      digits.insert( new LHCb::VPDigit(), channel );
      ++numNoiseDigitsCreated;
    }
  }

  // add the values from this event to the running totals
  m_numSpillover += numSpillover;
  m_numLostTiming += numLostTiming;
  m_numDigitsKilledMasked += numDigitsKilledMasked;
  m_numSignalDigitsCreated += ( numSignalDigitsCreated - numSpillover );
  m_numNoiseDigitsCreated += numNoiseDigitsCreated;

  std::sort( digits.begin(), digits.end(), VPDataFunctor::Less_by_Channel<const LHCb::VPDigit*>() );
  return digits;
}
